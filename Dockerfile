FROM rust:1.77-bullseye AS builder

RUN apt update
RUN apt install -y protobuf-compiler

WORKDIR /usr/src/toporacle

COPY . .
RUN cargo install --path .

FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y ca-certificates && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/cargo/bin/toporacle /usr/local/bin/toporacle

EXPOSE 3000
EXPOSE 33000

CMD ["toporacle"]