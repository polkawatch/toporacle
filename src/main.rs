// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use crate::cache::Cache;
use crate::network::TopNetwork;
use std::process;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::broadcast;
use tokio::time::sleep;
use tokio::{join, select};
use tracing::{error, info};
use tracing_subscriber::fmt;

mod cache;
mod chain;
mod cli;
mod network;
mod oapi;
mod types;

// The main module will simply capture command line parameters and start all stacks.

// Concurrency:

// This project inflicts little computational load, and should be possible to run as a
// pure async server

//#[tokio::main(flavor="multi_thread")]
#[tokio::main(flavor = "current_thread")]
async fn main() {
    // Command Line Options, that can be shared.
    let args = Arc::new(cli::parse());

    // Set up the subscriber for tracing/logs
    tracing_subscriber::fmt()
        .with_timer(fmt::time::uptime())
        .with_thread_names(args.log_threads.clone())
        .with_thread_ids(args.log_threads.clone())
        .with_env_filter(args.log.clone())
        .with_ansi(args.log_color.clone())
        .init();

    if args.spec {
        info!(
            "Polkawatch {} generating openapi.json specification",
            env!("CARGO_PKG_NAME")
        );
        oapi::spec();
        info!("Done.");
    } else {
        info!("Polkawatch {} starting...", env!("CARGO_PKG_NAME"));

        // We will use a broadcast signal to shutdown all services. Rocket has good signal processing
        // So we can just broadcast to the rest when Rocket stops.
        let (shutdown_tx, _) = broadcast::channel(10);

        // Create the Chain Monitor, and capture the chain info

        let chain_cfg = args.clone();
        let mut cm = chain::ChainMonitor::new(chain_cfg, shutdown_tx.clone())
            .await
            .expect("Could not create Chain Monitor");
        let chain_info = Arc::new(
            cm.get_chain_info()
                .await
                .expect("Could not retrieve chain info"),
        );

        let chain_shutdown_tx = shutdown_tx.clone();
        let chain_task = tokio::spawn(async move {
            info!("Launching Chain Monitor...");
            if let Err(e) = cm.monitor().await {
                error!("Chain Monitor exited with error {e}");
                // shutdown all
                _ = chain_shutdown_tx.send(());
            }
        });

        // Create the OpenAPI Server

        let oapi_cfg = args.clone();
        let mut opai_shutdown_rx = shutdown_tx.subscribe();
        let oapi_shutdown_tx = shutdown_tx.clone();
        let oapi_chain_info = chain_info.clone();
        let oapi_task = tokio::spawn(async move {
            info!("Launching OpenAPI/Rocket...");
            let cache = Cache::new(oapi_cfg.clone(), oapi_chain_info.clone())
                .await
                .expect("Could not create cache");
            let rocket = oapi::server(oapi_cfg, oapi_chain_info, cache)
                .ignite()
                .await
                .expect("Could not configure rocket");
            let rocket_shutdown = rocket.shutdown();
            select!(
                _ = opai_shutdown_rx.recv() => {
                    // another stack exited or failed
                    info!("Shutting Down OpenAPI...");
                    rocket_shutdown.notify();
                }
                rocket_result = rocket.launch() => {
                    // rocket exited (signal) or failed
                    match rocket_result {
                        Ok(_) => info!("OpenAPI shut down gracefully."),
                        Err(err) => error!("Rocket exited with error: {err}"),
                    };
                    // Rocket exists on OS signals or error, we will notify termination when rocket exists
                    _ = oapi_shutdown_tx.send(());
                }
            );
        });

        // Create the P2P Network

        let top_network = TopNetwork::new(args.clone(), chain_info.clone(), shutdown_tx.clone())
            .await
            .expect("Could not start p2p network");
        let network_shutdown_tx = shutdown_tx.clone();
        let network_task = tokio::spawn(async move {
            info!("Launching libp2p...");
            if let Err(e) = top_network.wait_for_tasks().await {
                error!("Libp2p Network exited with error {e}");
                // shutdown all
                _ = network_shutdown_tx.send(());
            }
        });

        // Wait for a shutdown signal
        let mut main_shutdown_rx = shutdown_tx.subscribe();
        main_shutdown_rx
            .recv()
            .await
            .expect("Could not wait for shutdown");

        // A normal shutdown is triggered by an OS signal, wich is propagated starting by rocket.
        select! {
            _ = async { _ = join!(oapi_task, network_task, chain_task); } => {
                info!("Polkawatch {} shutdown successfully.", env!("CARGO_PKG_NAME"));
            }
            _ = sleep(Duration::from_secs(10)) => {
                info!("Polkawatch {} terminated.", env!("CARGO_PKG_NAME"));
                // abnormal termination exit code
                process::exit(1);
            }

        }
    }
}
