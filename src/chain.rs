// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

// The chain module will listen to chain events and read chain storage to determine
// the peers of interest

use std::sync::Arc;
use tracing::{debug, info, warn};

use chrono::Utc;
use std::time::Duration;

use rocket::serde::json::serde_json;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::Read;

use subxt::dynamic::Value;
use subxt::{Error, OnlineClient, PolkadotConfig};
use tokio::sync::broadcast::Sender;

use crate::cache::Cache;
use subp2p_explorer::util::crypto::sr25519;
use subxt::backend::legacy::rpc_methods::StorageKey;
use subxt::backend::legacy::LegacyRpcMethods;
use subxt::backend::rpc::RpcClient;
use subxt::blocks::Block;
use subxt::error::RpcError::ClientError;
use subxt::ext::codec::Decode;
use tokio::time;
use tokio::time::interval;

use crate::cli::Args;
use crate::types::{
    Authority, AuthorityInfo, AuthorityLocation, BlockAuthoring, Bootnodes, ChainInfo,
    ChainLocationStatus, KeyOwnerInfo, Task, TaskExecution, TopError,
};

// primitive definition of KeyTypes, to locate the key owner
pub const AUTHORITY_DISCOVERY_KTYPE: [u8; 4] = *b"audi";
pub const AURA_KTYPE: [u8; 4] = *b"aura";

// The bootnodes as stored in the spec file
#[derive(Debug, Serialize, Deserialize)]
pub struct BootNodesSpec {
    #[serde(rename = "bootNodes")]
    boot_nodes: Bootnodes,
}

pub struct ChainMonitor {
    /// The Gobal Configuration
    cfg: Arc<Args>,
    /// A Configured Online Client, available after async initialization
    online_client: OnlineClient<PolkadotConfig>,
    /// A Configured RPC Client, available after async initialization
    rpc_client: LegacyRpcMethods<PolkadotConfig>,

    /// The Cache
    cache: Option<Cache>,

    /// The Chain Info
    chain_info: Option<ChainInfo>,

    /// The shutdown sender
    shutdown_tx: Sender<()>,
}

impl ChainMonitor {
    // Simple sync constructor
    pub async fn new(cfg: Arc<Args>, shutdown_tx: Sender<()>) -> Result<Self, TopError> {
        let (online_client, rpc_client) = ChainMonitor::connect(cfg.clone()).await?;

        info!("Chain Monitor has been Initialized OK.");

        let mut cm = ChainMonitor {
            cfg: cfg.clone(),
            online_client,
            rpc_client,
            cache: None,
            chain_info: None,
            shutdown_tx,
        };

        // Include the cache
        let ci = cm.get_chain_info().await?;
        cm.chain_info = Some(ci.clone());
        cm.cache = Some(Cache::new(cfg.clone(), Arc::new(ci)).await?);
        Ok(cm)
    }

    async fn connect(
        cfg: Arc<Args>,
    ) -> Result<
        (
            OnlineClient<PolkadotConfig>,
            LegacyRpcMethods<PolkadotConfig>,
        ),
        TopError,
    > {
        // Initialize the Online Client
        let online_client = OnlineClient::<PolkadotConfig>::from_url(cfg.rpc.clone()).await?;

        // Initialize the Legacy RPC Client
        let client = RpcClient::from_url(cfg.rpc.clone()).await?;

        let rpc_client = LegacyRpcMethods::<PolkadotConfig>::new(client);

        Ok((online_client, rpc_client))
    }

    // The experimental reconnecting client did not work as expected
    // We reconnect by hand.
    async fn reconnect(&mut self) -> Result<(), TopError> {
        let (online_client, rpc_client) = ChainMonitor::connect(self.cfg.clone()).await?;
        self.rpc_client = rpc_client;
        self.online_client = online_client;
        Ok(())
    }

    // Retrieves the curren set of autorities from chain storage
    pub async fn get_authorities(&self) -> Result<Vec<Authority>, TopError> {
        let api = &self.online_client;

        let result = match self.get_chain_info().await?.block_authoring {
            BlockAuthoring::BABE => {
                let runtime_api_call = subxt::dynamic::runtime_api_call(
                    "AuthorityDiscoveryApi",
                    "authorities",
                    Vec::<Value>::new(),
                );
                api.runtime_api()
                    .at_latest()
                    .await?
                    .call(runtime_api_call)
                    .await?
            }
            BlockAuthoring::AURA => {
                let storage_query =
                    subxt::dynamic::storage("Aura", "Authorities", Vec::<Value>::new());
                api.storage()
                    .at_latest()
                    .await?
                    .fetch(&storage_query)
                    .await?
                    .expect("Empty Set of AURA Authorities found")
            }
        };

        let bytes = result.into_encoded();
        let authorities: Vec<sr25519::PublicKey> = Decode::decode(&mut &bytes[..])?;
        let authorities: Vec<Authority> = authorities.iter().map(|a| a.clone().into()).collect();

        Ok(authorities)
    }

    // Returns the PublicKey of the owner for an
    // Authority Discovery key
    pub async fn key_owner(&self, a: Authority) -> Result<Option<Authority>, TopError> {
        let api = &self.online_client;

        // The Key type to search depending of the block authoring
        let authority_key_type = match self.get_chain_info().await?.block_authoring {
            BlockAuthoring::BABE => AUTHORITY_DISCOVERY_KTYPE,
            BlockAuthoring::AURA => AURA_KTYPE,
        };

        let key: sr25519::PublicKey = a.into();

        let query = subxt::dynamic::storage(
            "Session",
            "KeyOwner",
            vec![
                Value::from_bytes(authority_key_type),
                Value::from_bytes(key),
            ],
        );

        match api.storage().at_latest().await?.fetch(&query).await? {
            None => Ok(None),
            Some(value) => {
                // Ensure the decoded identifier (public key) matches in size
                let value = value
                    .encoded()
                    .get(..32)
                    .ok_or(TopError::ChainError("Invalid Owner Key Length".to_string()))?;

                // Ensure we can cast into a public key, owned instead of referenced, and also
                // deal with other error trait
                match <&sr25519::PublicKey>::try_from(value) {
                    Ok(key) => Ok(Some((*key).clone().into())),
                    _ => Err(TopError::ChainError("Invalid Owner Key".to_string())),
                }
            }
        }
    }

    pub async fn ss58_format(&self) -> Result<u16, TopError> {
        let api = &self.online_client;
        let query = subxt::dynamic::constant("System", "SS58Prefix");
        let value = api.constants().at(&query)?;
        let value = value.to_value()?;
        let value = value.as_u128().expect("Value Error");
        Ok(value as u16)
    }

    // Return the Genesis hash as human readable string "0x..."
    pub async fn genesis(&self) -> Result<String, TopError> {
        let api = &self.online_client;
        let result = api.genesis_hash();
        Ok(format!("0x{}", hex::encode(result)))
    }

    pub async fn read_boot_nodes(&self) -> Result<BootNodesSpec, TopError> {
        let spec_file_name = format!(
            "{}/{}.spec.json",
            &self.cfg.dir,
            self.get_chain_code().await?
        );
        let mut spec = File::open(spec_file_name.clone())
            .expect(&*format!("file {} not found", spec_file_name));
        let mut content = String::new();
        spec.read_to_string(&mut content)
            .expect("could not read chain spec");

        let content = content.replace("\r\n", "\n");

        let boot_nodes = serde_json::from_str::<BootNodesSpec>(&content);
        Ok(boot_nodes.expect("Could not parse chain spec"))
    }

    // Returns the chain name
    async fn get_chain_name(&self) -> Result<String, TopError> {
        let rpc = &self.rpc_client;
        Ok(rpc.system_chain().await?)
    }

    async fn get_chain_code(&self) -> Result<String, TopError> {
        Ok(self
            .get_chain_name()
            .await?
            .to_lowercase()
            .replace(" ", "_"))
    }

    async fn get_block_authoring(&self) -> Result<BlockAuthoring, TopError> {
        // We test for each tech
        let api = &self.online_client;

        // check for babe
        let babe_query = subxt::dynamic::constant("Babe", "MaxAuthorities");
        let babe_result = api.constants().at(&babe_query);

        // check for aura
        let aura_query = subxt::dynamic::storage("Aura", "Authorities", Vec::<StorageKey>::new());
        let aura_result = api.storage().at_latest().await?.fetch(&aura_query).await;

        // analyse the results
        match (babe_result, aura_result) {
            (Ok(_), _) => {
                info!("Block Authoring is BABE");
                Ok(BlockAuthoring::BABE)
            }
            (_, Ok(_)) => {
                info!("Block Authoring is AURA");
                Ok(BlockAuthoring::AURA)
            }
            _ => Err(TopError::ChainError(
                "Block Authoring Not Detected".to_string(),
            )),
        }
    }

    pub async fn get_chain_info(&self) -> Result<ChainInfo, TopError> {
        if let Some(chain_info) = self.chain_info.clone() {
            Ok(chain_info)
        } else {
            Ok(ChainInfo {
                name: self.get_chain_name().await?,
                code: self.get_chain_code().await?,
                ss58_format: self.ss58_format().await?,
                boot_nodes: self.read_boot_nodes().await?.boot_nodes,
                genesis: self.genesis().await?,
                block_authoring: self.get_block_authoring().await?,
            })
        }
    }

    // Process the new block and file new tasks if required
    pub async fn process_block(
        &self,
        block: Result<Block<PolkadotConfig, OnlineClient<PolkadotConfig>>, Error>,
    ) -> Result<(), TopError> {
        let block = block?;
        let block_number = block.header().number;
        let block_hash = block.hash();

        if block_number % 10 == 0 {
            info!(
                "{} Reached Block #{block_number} ({block_hash})",
                self.get_chain_info().await?.name
            );
        } else {
            debug!("Block #{block_number} ({block_hash})");
        }

        // TODO: Detect key changes and add tasks accordingly
        // Currently authorities are listed every X hours

        Ok(())
    }

    /// Trigger tasks for each authority and returns the currently known information
    /// for each, so that statistics can be performed
    pub async fn process_authority(&self, a: Authority) -> Result<(), TopError> {
        // Get a cache reference
        let Some(cache) = &self.cache else {
            panic!("Invalid Cache State")
        };

        // Has the authority been already identified?
        // We look it up in the cache using the HEX string which is the ID
        let ci = cache.retrieve::<AuthorityInfo>(a.to_hex().as_str()).await?;

        let exp = self.cfg.p2p_task_exp_h;

        let next_task: Option<TaskExecution> = match ci {
            // Note that task expiry time has to be < than authority processing
            Some(ai) => {
                match ai.best_location {
                    AuthorityLocation::Unknown { .. } => {
                        Some(
                            // We try to discover it again
                            TaskExecution {
                                task: Task::Discover(a.clone()),
                                retries: 0,
                                expiry: Utc::now()
                                    + Duration::from_secs(exp as u64 * 60u64 * 60u64),
                            },
                        )
                    }
                    AuthorityLocation::Declared { .. } => {
                        // We attempt to connect to the peer
                        Some(
                            // We try to connec to the declared peer
                            TaskExecution {
                                task: Task::Identify(ai.id),
                                retries: 0,
                                expiry: Utc::now()
                                    + Duration::from_secs(exp as u64 * 60u64 * 60u64),
                            },
                        )
                    }
                    // We identified the authority but have not seen it in a day, identify it again
                    AuthorityLocation::Identified { time_stamp: t, .. }
                        if t > Utc::now() + Duration::from_secs(24 * 60 * 60) =>
                    {
                        Some(
                            // We try to connect to the declared peer
                            TaskExecution {
                                task: Task::Identify(ai.id),
                                retries: 0,
                                expiry: Utc::now()
                                    + Duration::from_secs(exp as u64 * 60u64 * 60u64),
                            },
                        )
                    }
                    _ => None,
                }
            }
            // Otherwise we will
            None => {
                // lets find out which account owns this authority
                if let Some(owner) = self.key_owner(a.clone()).await? {
                    let owner_ss58 = owner.to_ss58(self.get_chain_info().await?.ss58_format);

                    let ai = AuthorityInfo::new_owned(a.clone(), Some(owner_ss58.clone()));

                    // We store what we know about this authority
                    cache.store(&ai).await?;

                    // We also store the association keyowner - authority discovery key
                    cache
                        .store(&KeyOwnerInfo::new(owner_ss58.clone(), a.clone()))
                        .await?;

                    // We request the discovery of the authority
                    // the network will take care of it
                    Some(TaskExecution {
                        task: Task::Discover(a.clone()),
                        retries: 0,
                        expiry: Utc::now() + Duration::from_secs(exp as u64 * 60u64 * 60u64),
                    })
                } else {
                    warn!("Missing KeyOwner of Authority: {a}");
                    None
                }
            }
        };

        if let Some(te) = next_task {
            cache.enqueue(&te).await?;
        } else {
            debug!("No task required for authority {a}");
        }
        Ok(())
    }

    // Process our authorities
    pub async fn process_authorities(&self) -> Result<(), TopError> {
        // Get a cache reference
        let Some(cache) = &self.cache else {
            panic!("Invalid Cache State")
        };

        let auths = &self.get_authorities().await?;

        for a in auths {
            self.process_authority(a.clone()).await?;
            if cache.queue_length::<TaskExecution>().await? > auths.len() as u32 {
                info!("Task queue is full, will wait for next authority process.");
                break;
            }
        }

        info!("{} authorities found on chain, tasks updated.", auths.len());

        Ok(())
    }

    pub async fn report_authority_stats(&self) -> Result<(), TopError> {
        // Get a cache reference
        let Some(cache) = &self.cache else {
            panic!("Invalid Cache State")
        };

        let auths = &self.get_authorities().await?;
        let (mut total, mut unknown, mut declared, mut identified) = (0u32, 0u32, 0u32, 0u32);
        for a in auths {
            if let Some(ai) = cache.retrieve::<AuthorityInfo>(a.to_hex().as_str()).await? {
                match ai.best_location {
                    AuthorityLocation::Unknown { .. } => unknown += 1,
                    AuthorityLocation::Declared { .. } => declared += 1,
                    AuthorityLocation::Identified { .. } => identified += 1,
                }
            } else {
                unknown += 1;
            }
            total += 1;
        }

        info!("STATS: {total} authorities, with locations: {unknown} unknown, {declared} declared, {identified} identified.");

        let queued_p2p_tasks = cache.queue_length::<TaskExecution>().await?;

        cache
            .store::<ChainLocationStatus>(&ChainLocationStatus {
                authorities: total,
                unknown,
                declared,
                identified,
                queued_p2p_tasks,
            })
            .await?;

        Ok(())
    }

    // Monitor the blockchain until a shutdown signal is received
    pub async fn monitor(&mut self) -> Result<(), TopError> {
        let mut api = &self.online_client;

        // crate a shutdown receiver
        let mut shutdown = self.shutdown_tx.subscribe();

        let info = self.get_chain_info().await?;

        info!("Connected to chain: {}({})", info.name, info.code);
        info!("Chain genesis is: {}", info.genesis);
        info!("Chain ss58 format is: {}", info.ss58_format);

        // Crate job intervals
        let mut process_authorities_interval = interval(Duration::from_secs(
            self.cfg.authority_check_int_h as u64 * 60u64 * 60u64,
        ));
        let mut report_stats_interval = interval(Duration::from_secs(60));

        // Subscribe to all finalized blocks:
        let mut blocks_sub = api.blocks().subscribe_finalized().await?;

        loop {
            let result;

            tokio::select! {
                // process all authorities on fixed interval
                _ = process_authorities_interval.tick() => {
                    result = self.process_authorities().await;
                }

                // report statistics as often as tasks are run
                _ = report_stats_interval.tick() => {
                    result = self.report_authority_stats().await;
                }

                // We listen to block and trigger tasks accordingly
                Some(block) = blocks_sub.next() => {
                    result = self.process_block(block).await;
                }

                // We wait for a shutdown signal
                _ = shutdown.recv() => {
                    info!("Chain Monitor shutting down...");
                    break;
                }

            }

            // Catch protocol errors and reconnect if necessary
            match result {
                Err(TopError::SubstrateError(subxt::Error::Rpc(ClientError(_)))) => {
                    'reconnect: loop {
                        match self.reconnect().await {
                            Ok(_) => {
                                info!("RPC Client reconnected!");
                                // re-subscribe to block notifications, again
                                api = &self.online_client;
                                blocks_sub = api.blocks().subscribe_finalized().await?;
                                break 'reconnect;
                            }
                            Err(e) => {
                                warn!("RPC Connection failed, retrying in 2 seconds... {}", e);
                                time::sleep(Duration::from_secs(2)).await;
                            }
                        }
                    }
                }
                Err(e) => {
                    warn!("Error in chain monitor loop: {e:?}");
                }
                _ => (),
            }
        }

        Ok(())
    }
}

// These tests require a connection to the default Blockchain, that is Westend
// technically they are more integration than unit tests,

#[cfg(test)]
mod test {
    use super::*;
    use clap::Parser;
    use tokio::sync::broadcast;

    // Test data from Westend
    // Pick one current Authority Key, this will break when keys are rotated
    static TEST_AUTH_KEY_SSF: u16 = 42;
    static TEST_AUTH_KEY: &str =
        "0x0ec5e1d2d044023009c63659c65a79aaf07ecbf5b9887958243aa873a63e5a1b";
    static TEST_AUTH_KEY_OWNER: &str = "5E2CYS4D6KdD1nDh5d7hTtss3TR8etx4i92ozipJt5QtR9KY";
    pub static WESTNEND_GENESIS: &str =
        "0xe143f23803ac50e8f6f8e62695d1ce9e4e1d68aa36c1cd2cfd15340213f3423e";

    // A default configuration for testing
    pub fn default_cfg() -> Arc<Args> {
        Arc::new(Args::parse_from(vec!["toporacle", "-d", "./src/test/data"]))
    }

    pub async fn create_chain_monitor() -> ChainMonitor {
        let cfg = default_cfg();
        let (shutdown_tx, _) = broadcast::channel(10);
        let cm = ChainMonitor::new(cfg, shutdown_tx).await.unwrap();
        cm
    }

    #[tokio::test]
    async fn can_read_bootnodes_from_spec() {
        let cm = create_chain_monitor().await;
        let nodes = cm.read_boot_nodes().await.unwrap();
        assert!(nodes.boot_nodes.len() > 0);
    }

    #[tokio::test]
    async fn can_get_the_authorities() {
        let cm = create_chain_monitor().await;
        let authorities = cm
            .get_authorities()
            .await
            .expect("Could not get Authorities");
        let _authorities_ss58: Vec<String> = authorities.iter().map(|ak| ak.to_ss58(0)).collect();
        // Authority Keys are displayed in this format in Polkadot Apps
        let authorities_hex: Vec<String> = authorities.iter().map(|ak| ak.to_hex()).collect();

        assert!(authorities_hex.contains(&TEST_AUTH_KEY.to_string()));
    }

    #[tokio::test]
    async fn can_get_network_ss58_format() {
        let cm = create_chain_monitor().await;
        let value = cm.ss58_format().await.expect("Error getting ss58 format");
        assert_eq!(value, TEST_AUTH_KEY_SSF)
    }

    #[tokio::test]
    async fn can_get_the_authority_key_owner() {
        let cm = create_chain_monitor().await;
        let authority: Authority = TEST_AUTH_KEY.parse().expect("Invalid Hex String");

        let owner = cm
            .key_owner(authority)
            .await
            .expect("No Owner Found")
            .unwrap();
        let ssf = cm.ss58_format().await.expect("Could not Get SSF format");
        let owner = owner.to_ss58(ssf);
        assert_eq!(owner, TEST_AUTH_KEY_OWNER)
    }

    #[tokio::test]
    async fn can_get_the_chain_genesis() {
        let cm = create_chain_monitor().await;
        let genesis = cm.genesis().await.expect("Could not retrieve genesis");
        assert_eq!(genesis, WESTNEND_GENESIS);
    }

    #[tokio::test]
    async fn can_get_the_chain_name() {
        let cm = create_chain_monitor().await;
        let name = cm
            .rpc_client
            .system_chain()
            .await
            .expect("Could not get chain name");
        assert_eq!(name, "Westend")
    }
}
