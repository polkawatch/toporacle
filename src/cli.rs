// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use clap::Parser;

// Command Line Arguments

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
pub(crate) struct Args {
    /// Generate the openapi.json specification and exit
    #[arg(
        short = 's',
        long = "spec",
        env = "TOPO_OAPI_SPEC",
        default_value = "false"
    )]
    pub spec: bool,

    /// The node RPC url to connect to.
    #[arg(
        short = 'r',
        long = "rpc",
        env = "TOPO_RPC",
        default_value = "wss://westend-rpc.polkadot.io:443"
    )]
    pub rpc: String,

    /// The directory where data, including spec files are located. Chain spec files must be named
    /// chain.spec.json
    #[arg(short = 'd', long = "dir", env = "TOPO_DIR", default_value = "./")]
    pub dir: String,

    /// The directory where geoip data is.
    #[arg(
        short = 'G',
        long = "geo_dir",
        env = "TOPO_GEOIP_DIR",
        default_value = "./"
    )]
    pub geoip_dir: String,

    /// Premium GeoIP Web Service Account ID. Will be used instead of DB if provided.
    /// Must also provide the license key
    #[arg(short = 'A', long = "geo_aid", env = "TOPO_GEOIP_ID")]
    pub geoip_id: Option<String>,

    /// Premium GeoIP Web Service License Key. Will be used instead of DB if provided.
    /// Must also provide the account ID
    #[arg(short = 'L', long = "geo_key", env = "TOPO_GEOIP_KEY")]
    pub geoip_key: Option<String>,

    /// Premium GeoIP Web Service Cache Expiry in Seconds.
    #[arg(
        short = 'Y',
        long = "geo_cache_expiry",
        env = "TOPO_GEOIP_CACHE_EXPIRY",
        default_value = "3600"
    )]
    pub geoip_cache_expiry: u32,

    /// The OpenAPI Server Port
    #[arg(
        short = 'a',
        long = "api_port",
        env = "TOPO_API_PORT",
        default_value = "3000"
    )]
    pub api_port: u16,

    /// The P2P Network Port
    #[arg(
        short = 'p',
        long = "p2p_port",
        env = "TOPO_P2P_PORT",
        default_value = "33000"
    )]
    pub p2p_port: u16,

    /// The Log setup, equivalent to RUST_LOG
    #[arg(short = 'l', long = "log", env = "TOPO_LOG", default_value = "info")]
    pub log: String,

    /// The Color Log setup
    #[arg(
        short = 'C',
        long = "color",
        env = "TOPO_LOG_COLOR",
        default_value = "true"
    )]
    pub log_color: bool,

    /// Whether to log thread information or not
    #[arg(
        short = 't',
        long = "threads",
        env = "TOPO_LOG_THREADS",
        default_value = "false"
    )]
    pub log_threads: bool,

    /// The redis url to connect to.
    #[arg(
        short = 'e',
        long = "redis",
        env = "TOPO_REDIS",
        default_value = "redis://127.0.0.1/"
    )]
    pub redis: String,

    /// The Maximum Network Tasks to Launch on each run. Recommended setting is 1/3 of number of authorities, max 300 min 10.
    #[arg(
        short = 'b',
        long = "p2p_batch",
        env = "TOPO_P2P_BATCH",
        default_value = "100"
    )]
    pub p2p_task_batch: u16,

    /// For how long are tasks executed in the p2p network, seconds. Uncompleted tasks will be retried later. Most tasks are executed in 60 seconds.
    #[arg(
        short = 'x',
        long = "p2p_exec_s",
        env = "TOPO_P2P_EXEC_S",
        default_value = "120"
    )]
    pub p2p_exec_time_s: u16,

    /// The task expiry in hours. Tasks are abandoned after this time if not completed. They may be re-instantiated by authority processing later.
    #[arg(
        short = 'X',
        long = "task_exp_h",
        env = "TOPO_TASK_EXP_H",
        default_value = "20"
    )]
    pub p2p_task_exp_h: u16,

    /// Launch pending p2p network tasks at this interval, minutes, if there are pending tasks.
    #[arg(
        short = 'I',
        long = "task_int_m",
        env = "TOPO_TASK_INT_M",
        default_value = "15"
    )]
    pub p2p_task_launch_int_m: u16,

    /// Chain Authority Process interval hours. The blockchain is scanned for authorities proactively at this interval.
    #[arg(
        short = 'K',
        long = "auth_chk_h",
        env = "TOPO_AUTH_CHK_H",
        default_value = "12"
    )]
    pub authority_check_int_h: u16,
}

// delegates the Call to CLAP
pub(crate) fn parse() -> Args {
    Args::parse()
}
