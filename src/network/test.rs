use clap::Parser;
use futures::StreamExt;
use libp2p::kad::{Event, GetRecordOk, KademliaEvent, QueryResult};
use libp2p::swarm::{DialError, SwarmEvent};
use libp2p::{identity, Multiaddr, PeerId, Swarm};
use std::sync::Arc;
use subp2p_explorer::peer_behavior::PeerInfoEvent;
use tokio::time::{sleep, Duration};

use crate::cli::Args;
use crate::network::swarm::{build_swarm, Behaviour, BehaviourEvent};
use crate::types::TopError;
use subp2p_explorer::peer_behavior::PeerInfoEvent::Identified;
use subp2p_explorer::util::authorities::{decode_dht_record, hash_authority_id};
use subp2p_explorer::util::crypto::sr25519;
use subxt::ext::codec::Decode;

// Some Well Known Peers for Testing
pub struct TestPeer {
    pub id: &'static str,
    pub addr: &'static str,
    pub ws_addr: &'static str,
}

pub static WESTNEND_GENESIS: &str =
    "0xe143f23803ac50e8f6f8e62695d1ce9e4e1d68aa36c1cd2cfd15340213f3423e";

// These are bootnodes

pub static WESTEND_P1: TestPeer = TestPeer {
    id: "12D3KooWKer94o1REDPtAhjtYR4SdLehnSrN8PEhBnZm5NBoCrMC",
    addr: "/dns/westend-bootnode-0.polkadot.io/tcp/30333/p2p/12D3KooWKer94o1REDPtAhjtYR4SdLehnSrN8PEhBnZm5NBoCrMC",
    ws_addr: "/dns/westend-bootnode-0.polkadot.io/tcp/30334/ws/p2p/12D3KooWKer94o1REDPtAhjtYR4SdLehnSrN8PEhBnZm5NBoCrMC",
};

pub static WESTEND_P2: TestPeer = TestPeer {
    id: "12D3KooWK8fjVoSvMq5copQYMsdYreSGPGgcMbGMgbMDPfpf3sm7",
    addr: "/dns/boot.stake.plus/tcp/32333/p2p/12D3KooWK8fjVoSvMq5copQYMsdYreSGPGgcMbGMgbMDPfpf3sm7",
    ws_addr: "/dns/boot.stake.plus/tcp/32334/wss/p2p/12D3KooWK8fjVoSvMq5copQYMsdYreSGPGgcMbGMgbMDPfpf3sm7",
};

// A peer to find
pub static WESTEND_PF: TestPeer = TestPeer {
    id: "12D3KooWDg1YEytdwFFNWroFj6gio4YFsMB3miSbHKgdpJteUMB9",
    addr: "/dns/ibp-boot-westend.luckyfriday.io/tcp/30333/p2p/12D3KooWDg1YEytdwFFNWroFj6gio4YFsMB3miSbHKgdpJteUMB9",
    ws_addr: "/dns/ibp-boot-westend.luckyfriday.io/tcp/30334/wss/p2p/12D3KooWDg1YEytdwFFNWroFj6gio4YFsMB3miSbHKgdpJteUMB9",
};

// An Authority key to find a peer for
pub static WESTEND_AUTH_KEY: &str =
    "0x0ec5e1d2d044023009c63659c65a79aaf07ecbf5b9887958243aa873a63e5a1b";

// We mock the default configuration, we dont have mandatory arguments, so...
pub fn mock_default_cfg() -> Arc<Args> {
    Arc::new(Args::parse_from(vec!["toporacle"]))
}

pub fn mock_swarm() -> Result<Swarm<Behaviour>, TopError> {
    build_swarm(
        mock_default_cfg(),
        WESTNEND_GENESIS,
        identity::Keypair::generate_ed25519(),
        vec![WESTEND_P1.addr.to_string()],
    )
}

// MANUAL TESTS: Unused code is left behind as can still be useful as implementation pattern.

// PING Tests

#[tokio::test]
#[cfg(feature = "manual_tests")]
async fn can_ping_a_well_known_p2p_node() {
    let mut swarm = mock_swarm().expect("SDK Swarm");

    // Pick a well known peer

    let test_peer = &WESTEND_P2;

    // p2p network listen
    swarm
        .listen_on(
            format!("/ip4/0.0.0.0/tcp/{}", 30373)
                .parse()
                .expect("Valid Multiaddr"),
        )
        .expect("Listening on port");

    let peer_ma: Multiaddr = test_peer.ws_addr.parse().expect("Valid Multiaddress; qed");

    swarm.dial(peer_ma).expect("Can Dial Peer");

    let mut peer_identified = false;

    // We process until peer identified
    loop {
        let event = swarm.select_next_some().await;
        match event {
            SwarmEvent::NewListenAddr { address, .. } => println!("Listening on {address:?}"),
            SwarmEvent::ConnectionClosed {
                connection_id,
                cause: Some(error),
                ..
            } => {
                println!("ConnectionId {connection_id:?} closed cause {error:?}");
            }
            SwarmEvent::OutgoingConnectionError {
                connection_id,
                peer_id,
                error,
            } => {
                println!("Outgoing connection to peer {peer_id:?} failed with error {error:?}");
            }
            SwarmEvent::Behaviour(BehaviourEvent::Discovery(event)) => {
                println!("Discovery event received {event:?}");
            }
            SwarmEvent::Behaviour(BehaviourEvent::PeerInfo(Identified { peer_id, .. })) => {
                println!("Peer Identified {peer_id:?}");
                if peer_id == test_peer.id.parse().unwrap() {
                    peer_identified = true;
                    break;
                }
            }
            SwarmEvent::Behaviour { .. } => {
                println!("Unknown behaviour event received {event:?}");
            }
            _ => {
                println!("Unknown SwarmEvent received {event:?}");
            }
        }
    }

    assert_eq!(peer_identified, true);
}

#[tokio::test]
#[cfg(feature = "manual_tests")]
async fn can_ping_a_well_known_ws_node() {
    let mut swarm = mock_swarm().expect("SDK Swarm");

    // Pick a well known peer

    let test_peer = &WESTEND_P2;

    // p2p network listen
    swarm
        .listen_on(
            format!("/ip4/0.0.0.0/tcp/{}", 30374)
                .parse()
                .expect("Valid Multiaddr"),
        )
        .expect("Listening on port");

    let peer_ma: Multiaddr = test_peer.ws_addr.parse().expect("Valid Multiaddress; qed");

    swarm.dial(peer_ma).expect("Can Dial Peer");

    let mut peer_identified = false;

    // We process until peer identified
    loop {
        let event = swarm.select_next_some().await;
        match event {
            SwarmEvent::NewListenAddr { address, .. } => println!("Listening on {address:?}"),
            SwarmEvent::ConnectionClosed {
                connection_id,
                cause: Some(error),
                ..
            } => {
                println!("ConnectionId {connection_id:?} closed cause {error:?}");
            }
            SwarmEvent::Behaviour(BehaviourEvent::Discovery(event)) => {
                println!("Discovery event received {event:?}");
            }
            SwarmEvent::Behaviour(BehaviourEvent::PeerInfo(Identified { peer_id, .. })) => {
                println!("Peer Identified {peer_id:?}");
                if peer_id == test_peer.id.parse().unwrap() {
                    peer_identified = true;
                    break;
                }
            }
            _ => {
                println!("Unknown SwarmEvent received {event:?}");
            }
        }
    }

    assert_eq!(peer_identified, true);
}

#[tokio::test]
#[cfg(feature = "manual_tests")]
async fn can_discover_a_well_known_node() {
    let mut swarm = mock_swarm().expect("SDK Swarm");

    // Pick a well known peer

    let test_peer = &WESTEND_PF;

    // p2p network listen

    swarm
        .listen_on(
            format!("/ip4/0.0.0.0/tcp/{}", 30375)
                .parse()
                .expect("Valid Multiaddr"),
        )
        .expect("Listening on port");

    // Will search by ID only
    let pf: PeerId = test_peer.id.to_string().parse().unwrap();

    // request the discovery
    swarm.behaviour_mut().discovery.get_closest_peers(pf);

    let mut peer_identified = false;

    loop {
        let event = swarm.select_next_some().await;

        // println!("++++ {event:?}");

        match event {
            SwarmEvent::NewListenAddr { address, .. } => println!("Listening on {address:?}"),
            SwarmEvent::Dialing {
                peer_id: Some(peer_id),
                connection_id,
            } => {
                if peer_id == pf {
                    // TODO: How can we be dialling our peer wihtout an address?
                    // a routing event should have come first, or?
                    // not clear in which order events are received
                    println!("DIALING {peer_id:?} via connection {connection_id:?}")
                }
            }
            SwarmEvent::ConnectionClosed {
                connection_id,
                cause: Some(error),
                ..
            } => {
                println!("ConnectionId {connection_id:?} closed cause {error:?}");
            }
            SwarmEvent::OutgoingConnectionError {
                connection_id,
                peer_id: Some(peer_id),
                error: DialError::Transport(transport),
            } => {
                if peer_id == pf {
                    // transport contains a vector of addresses and errors received
                    let addresses: Vec<&Multiaddr> =
                        transport.iter().map(|(address, error)| address).collect();
                    println!("PEER {peer_id:?} LOCATED AT {addresses:?} with failed connection");
                    peer_identified = true;
                    break;
                }
            }
            SwarmEvent::Behaviour(BehaviourEvent::Discovery(event)) => {
                match event {
                    // here we could re-start another query as one is already in progress
                    Event::OutboundQueryProgressed { .. } => {
                        println!("Our Query is progressing...");
                        swarm.behaviour_mut().discovery.get_closest_peers(pf);
                    }
                    Event::RoutingUpdated {
                        peer, addresses, ..
                    } => {
                        println!("Peer {peer:?} routable at addresses {addresses:?}");
                    }
                    Event::UnroutablePeer { peer } => {
                        println!("Peer is not routable {peer:?}");
                    }
                    _ => {
                        println!("Unknown Discovery event received {event:?}");
                    }
                }
            }
            SwarmEvent::Behaviour(BehaviourEvent::PeerInfo(Identified { peer_id, info })) => {
                if peer_id == pf {
                    let listen_addresses = info.listen_addrs;
                    println!("PEER IDENTIFIED {peer_id:?} at {listen_addresses:?}");
                    peer_identified = true;
                    break;
                }
            }
            _ => {
                //println!("Unknown SwarmEvent received {event:?}");
            }
        }
    }

    assert_eq!(peer_identified, true);
}

#[tokio::test]
#[cfg(feature = "manual_tests")]
async fn can_discover_a_well_known_authority() {
    let mut swarm = mock_swarm().expect("SDK Swarm");

    // Pick a well known peer

    let test_key_str = &WESTEND_AUTH_KEY;

    let test_key_hex = hex::decode(&test_key_str[2..]).expect("Invalid hex str");

    let test_key: sr25519::PublicKey =
        Decode::decode(&mut &test_key_hex[..]).expect("Invalid public key");

    let mut found = false;

    // p2p network listen

    swarm
        .listen_on(
            format!("/ip4/0.0.0.0/tcp/{}", 30376)
                .parse()
                .expect("Valid Multiaddr"),
        )
        .expect("Could not listen on port");

    let key = hash_authority_id(&test_key);

    // request the discovery
    let qid = swarm.behaviour_mut().discovery.get_record(key.clone());

    let peer_identified = false;

    loop {
        let event = swarm.select_next_some().await;

        println!("++++ {event:?}");

        match event {
            // Discovery DHT record.
            SwarmEvent::Behaviour(behavior_event) => {
                match behavior_event {
                    BehaviourEvent::Discovery(KademliaEvent::OutboundQueryProgressed {
                        id,
                        result: QueryResult::GetRecord(record),
                        ..
                    }) => {
                        // Has received at least one answer for this and can advance the queries.
                        match record {
                            Ok(GetRecordOk::FoundRecord(peer_record)) => {
                                let k = peer_record.record.key;
                                let value = peer_record.record.value;

                                if (k == key) {
                                    println!("Found a Value!");
                                    let (peer_id, _addresses) =
                                        match decode_dht_record(value, &test_key) {
                                            Ok((peer_id, addresses)) => {
                                                found = true;
                                                println!(
                                                    "Decoding DHT worked for authority {:?}: {:?}",
                                                    test_key, addresses
                                                );
                                                break;
                                                (peer_id, addresses)
                                            }
                                            Err(e) => {
                                                println!(
                                                    "Decoding DHT failed for authority {:?}: {:?}",
                                                    test_key, e
                                                );
                                                break;
                                            }
                                        };
                                }
                            }
                            _ => (),
                        }
                    }

                    BehaviourEvent::Discovery(KademliaEvent::OutboundQueryProgressed {
                        id,
                        result: QueryResult::GetClosestPeers(r),
                        ..
                    }) => {
                        println!("Query Progress {:?} {:?}", id, r)
                    }

                    BehaviourEvent::PeerInfo(info_event) => match info_event {
                        PeerInfoEvent::Identified { peer_id, info } => {
                            println!(" PeerInfo {} {:?}", peer_id, info);
                        }
                    },
                    _ => (),
                }
            }
            _ => (),
        }
    }

    assert_eq!(found, true);
}
