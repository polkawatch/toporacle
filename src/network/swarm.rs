// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use crate::cli::Args;
use crate::types::TopError;
use libp2p::identity::Keypair;

// TODO: how to build the swarm... perhaps will adopt the new stack earlier...
use libp2p::swarm::{NetworkBehaviour, SwarmBuilder};

use libp2p::{Multiaddr, PeerId, Swarm};
use std::sync::Arc;
use std::time::Duration;
use subp2p_explorer::discovery::DiscoveryBuilder;
use subp2p_explorer::notifications::behavior::ProtocolsData;
use subp2p_explorer::notifications::messages::ProtocolRole;
use subp2p_explorer::peer_behavior::PeerBehaviour;
use subp2p_explorer::transport::{TransportBuilder, MIB};
use subp2p_explorer::{discovery, peer_behavior};
use subxt::utils::H256;
use tracing::{debug, info};

#[derive(NetworkBehaviour)]
pub(crate) struct Behaviour {
    /// Implements Ping and Identity and stores peer information in a cache.
    pub peer_info: peer_behavior::PeerBehaviour,
    /// Discovers nodes of the network.
    pub discovery: discovery::Discovery,
}

#[allow(unused)]
pub fn build_swarm(
    cfg: Arc<Args>,
    genesis: &str,
    identity: Keypair,
    bootnodes: Vec<String>,
) -> Result<Swarm<Behaviour>, TopError> {
    let local_peer_id = PeerId::from(identity.public());
    info!("Local peer ID {:?}", local_peer_id);

    let genesis = genesis.trim_start_matches("0x");

    // Parse the provided bootnodes as `PeerId` and `MultiAddress`.
    let bootnodes: Vec<_> = bootnodes
        .iter()
        .map(|bootnode| {
            let parts: Vec<_> = bootnode.split('/').collect();
            let peer = parts.last().expect("Valid bootnode has peer; qed");
            let multiaddress: Multiaddr = bootnode.parse().expect("Valid multiaddress; qed");
            let peer_id: PeerId = peer.parse().expect("Valid peer ID; qed");

            debug!("Bootnode peer={:?}", peer_id);
            (peer_id, multiaddress)
        })
        .collect();

    info!(
        "Using {} bootnode addresses to create swarm",
        bootnodes.len()
    );

    // Craft the specific protocol data.
    let protocol_data = ProtocolsData {
        genesis_hash: H256::from_slice(hex::decode(genesis)?.as_slice()),
        node_role: ProtocolRole::FullNode,
    };

    // Create a Switch (swarm) to manage peers and events.
    let mut swarm: Swarm<Behaviour> = {
        let transport = TransportBuilder::new()
            .yamux_maximum_buffer_size(256 * MIB)
            .build(identity.clone());

        let discovery = DiscoveryBuilder::new()
            .record_ttl(Some(Duration::from_secs(0)))
            .provider_ttl(Some(Duration::from_secs(0)))
            .query_timeout(Duration::from_secs(5 * 60))
            .build(local_peer_id, genesis);

        let peer_info = PeerBehaviour::new(identity.public());

        let behavior = Behaviour {
            peer_info,
            discovery,
        };

        // Not sure how the idle connection timeout works... setting to less than 10 does not seem to change anything, but 60 will make it wait 50 seconds.
        SwarmBuilder::with_tokio_executor(transport, behavior, local_peer_id)
            .idle_connection_timeout(Duration::from_secs(60))
            .build()
    };

    // Active set of peers from the kbuckets of kademlia.
    // These are the initial peers for which the queries are performed against.
    for (peer, multiaddress) in &bootnodes {
        swarm
            .behaviour_mut()
            .discovery
            .add_address(peer, multiaddress.clone());
    }

    Ok(swarm)
}
