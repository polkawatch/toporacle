// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use async_std_resolver::{config, resolver, AsyncStdResolver};
use ipnet::IpNet;
use libp2p::multiaddr::Protocol;
use libp2p::Multiaddr;
use std::net::IpAddr;
use std::str::FromStr;
use tracing::warn;

use crate::types::TopError;

pub(crate) struct IPResolver {
    resolver: AsyncStdResolver,
}

impl IPResolver {
    pub(crate) async fn new() -> Self {
        IPResolver {
            resolver: resolver(
                config::ResolverConfig::default(),
                config::ResolverOpts::default(),
            )
            .await,
        }
    }

    // IP Utilities

    /// Resolves a set of multiaddresses into a set of public IP addresses
    /// Discards everything in the middle
    pub(crate) async fn resolve(
        &self,
        addresess: &Vec<Multiaddr>,
    ) -> Result<Vec<IpAddr>, TopError> {
        let mut ret = Vec::<IpAddr>::new();

        for addr in addresess {
            let oips = match addr.iter().next() {
                Some(Protocol::Ip4(ip)) => Some(vec![IpAddr::V4(ip)]),
                Some(Protocol::Ip6(ip)) => Some(vec![IpAddr::V6(ip)]),
                Some(Protocol::Dns(dns))
                | Some(Protocol::Dns4(dns))
                | Some(Protocol::Dns6(dns)) => {
                    match self.resolver.lookup_ip(dns.to_string()).await {
                        Ok(result) => Some(result.iter().map(|ip| ip).collect()),
                        Err(e) => {
                            warn!("DNS Resolver error: {e}");
                            None
                        }
                    }
                }
                _ => None,
            };
            if let Some(ips) = oips {
                for ip in ips {
                    if IPResolver::is_public_ip(ip) {
                        ret.push(ip)
                    }
                }
            }
        }
        Ok(ret)
    }

    /// Returns true if the ip does not belong to any private network
    fn is_public_ip(ip: IpAddr) -> bool {
        !PRIVATE_NETWORKS
            .iter()
            .any(|net| IpNet::from_str(net).unwrap().contains(&ip))
    }
}

/// All private networks
static PRIVATE_NETWORKS: [&str; 13] = [
    "10.0.0.0/8",
    "172.16.0.0/12",
    "192.168.0.0/16",
    "127.0.0.0/8",
    "169.254.0.0/16",
    "192.0.2.0/24",
    "198.18.0.0/15",
    "198.51.100.0/24",
    "203.0.113.0/24",
    "100.64.0.0/10",
    "::1/128",
    "fc00::/7",
    "fe80::/10",
];

#[cfg(test)]
mod test {
    use crate::network::ip::IPResolver;
    use libp2p::Multiaddr;

    #[tokio::test]
    async fn will_test_trouble_resolution() {
        // TODO: the failure was a collision on request port.
        // We assume a single resolver needs to be used per Async Core
        let addr:Multiaddr = "/dns/validator-0.kusama-nvme.tsukistaking.com/tcp/30333/p2p/12D3KooWRx9NMqij8sXKJAB9B1C4DZGdMjF7exWGc5pzpFt7NaGy".parse().expect("Invalid Address");
        let resolver = IPResolver::new().await;

        let ips = resolver
            .resolve(&vec![addr.clone(), addr.clone()])
            .await
            .expect("Could not resolve");

        assert!(ips.len() > 0);
    }
}
