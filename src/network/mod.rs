// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use crate::cache::Cache;
use chrono::Utc;
use futures::StreamExt;
use libp2p::core::ConnectedPoint;
use libp2p::kad::{Event as KademliaEvent, GetRecordOk, QueryId, QueryResult};
use libp2p::swarm::SwarmEvent;
use libp2p::{identity, PeerId, Swarm};
use std::collections::HashMap;
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;
use subp2p_explorer::peer_behavior::PeerInfoEvent::Identified;
use subp2p_explorer::util::authorities::{decode_dht_record, hash_authority_id};
use subp2p_explorer::util::crypto::sr25519;
use tokio::sync::broadcast::Sender;
use tokio::time::{interval, sleep};
use tracing::{debug, info, warn};

use crate::cli::Args;
use crate::network::geo::GeoIP;
use crate::network::ip::IPResolver;
use crate::network::swarm::{Behaviour, BehaviourEvent};
use crate::types::{
    AuthorityInfo, AuthorityLocation, ChainInfo, PeerInfo, Task, TaskExecution, TopError,
};

mod swarm;

#[cfg(feature = "manual_tests")]
mod test;

mod geo;
mod ip;

/// P2P Network implementation for TopOracle
/// This is not a full Polkadot Networking Node, as it advertises only
/// one capability, that related to identity.
/// We will try to limit its networking activity and keep it to the minimum
/// We assume that Polkadot Networking Stack can detect that this node does not
/// implement required services such as block announces, etc, however, we keep the
/// networking time to the strict minimum required.
pub struct TopNetwork {
    cfg: Arc<Args>,
    chain_info: Arc<ChainInfo>,
    cache: Cache,
    resolver: IPResolver,
    geo_ip: GeoIP,
    shutdown_tx: Sender<()>,
}

impl TopNetwork {
    pub async fn new(
        cfg: Arc<Args>,
        chain_info: Arc<ChainInfo>,
        shutdown_tx: Sender<()>,
    ) -> Result<Self, TopError> {
        let cache = Cache::new(cfg.clone(), chain_info.clone()).await?;
        let geo_ip = GeoIP::new(cfg.clone(), chain_info.clone()).await?;
        let resolver = IPResolver::new().await;
        Ok(TopNetwork {
            cfg,
            chain_info,
            cache,
            resolver,
            geo_ip,
            shutdown_tx,
        })
    }

    // Converts the boot_nodes in the format expected by the swarm
    fn swarm_bootnodes(&self) -> Vec<String> {
        self.chain_info
            .boot_nodes
            .iter()
            .map(|address| address.to_string())
            .collect()
    }

    // Starts a listening swarm
    pub async fn start_swarm(&self) -> Result<Swarm<Behaviour>, TopError> {
        // Will transport and behaviours
        let local_key = identity::Keypair::generate_ed25519();
        let _local_peer_id = PeerId::from(local_key.public());

        let mut swarm = swarm::build_swarm(
            self.cfg.clone(),
            &self.chain_info.genesis,
            local_key,
            self.swarm_bootnodes(),
        )?;

        // p2p network listen
        swarm.listen_on(format!("/ip4/0.0.0.0/tcp/{}", self.cfg.p2p_port).parse()?)?;

        Ok(swarm)
    }

    // Process the pending tasks, at this point we are certain there are tasks
    // Start the p2p network. The network client will connect to multiple peers and use the DHT to query
    // the address of the peers of interest, normally network authorities.

    async fn process_network_tasks(&self) -> Result<(), TopError> {
        // Create a shutdown receiver, in case the signal is received mid execution
        let mut shutdown = self.shutdown_tx.subscribe();

        // Query ID to task map
        let mut task_execs = HashMap::<QueryId, TaskExecution>::new();
        let mut discover_keys = HashMap::new();
        let mut connected_peer_addr = HashMap::new();

        // Create the swarm
        let mut swarm = self.start_swarm().await?;

        // dont overload the p2p network
        let max_tasks = self.cfg.p2p_task_batch;
        let mut current_batch = 0;

        // Create the tasks
        loop {
            if let Some(t) = self.cache.dequeue::<TaskExecution>().await? {
                match &t {
                    TaskExecution {
                        task: t, expiry: e, ..
                    } if e < &Utc::now() => {
                        warn!("Abandoning expired task: {t:?}");
                    }
                    TaskExecution {
                        task: Task::Discover(a),
                        ..
                    } => {
                        let key: sr25519::PublicKey = a.clone().into();
                        let key_hash = hash_authority_id(&key);
                        let qid = swarm.behaviour_mut().discovery.get_record(key_hash.clone());
                        task_execs.insert(qid, t.clone());
                        discover_keys.insert(qid, key);
                        info!(
                            "Starting authority discovery query {qid:?} for {a} attempt {}",
                            t.retries + 1
                        );
                    }
                    TaskExecution {
                        task: Task::Identify(a),
                        ..
                    } => {
                        info!(
                            "Starting authority identification for {a} attempt {}",
                            t.retries + 1
                        );
                        // get the location we know about this authority
                        match self
                            .cache
                            .retrieve::<AuthorityInfo>(a.to_hex().as_str())
                            .await?
                        {
                            Some(AuthorityInfo {
                                best_location: AuthorityLocation::Identified { address: a, .. },
                                ..
                            }) => {
                                info!("Starting identification refresh of authority: {a}");
                                if let Err(e) = swarm.dial(a.clone()) {
                                    debug!("Dial error {e:?}");
                                }
                            }
                            Some(AuthorityInfo {
                                best_location:
                                    AuthorityLocation::Declared {
                                        listening_addresses: ads,
                                        ..
                                    },
                                ..
                            }) => {
                                // TODO: no private addresses!
                                info!("Starting identification of authority: {a}");
                                for a in ads.iter() {
                                    if let Err(e) = swarm.dial(a.clone()) {
                                        debug!("Dial error {e:?}");
                                    }
                                }
                            }
                            None => {
                                warn!("Asked to identify an authority we know nothing about: {a}");
                            }
                            _ => {}
                        }
                    }
                    TaskExecution {
                        task: Task::Refresh(a),
                        ..
                    } => {
                        info!("Starting authority refresh for {a}");
                    }
                }
                current_batch += 1;
                // Limit the size of the batch
                if current_batch == max_tasks {
                    break;
                }
            } else {
                // no more tasks to run
                break;
            }
        }
        info!("Launched {} P2P Network Tasks...", current_batch);

        // Process Events until all tasks are done or timeout
        loop {
            tokio::select! {
               event = swarm.select_next_some() => {
                    match event {

                        // Generic local Events
                        SwarmEvent::NewListenAddr {
                            address, ..
                        } => debug!("Listening on {address:?}"),

                        SwarmEvent::ConnectionEstablished{
                            peer_id,
                            endpoint: ConnectedPoint::Dialer {
                                address,
                                ..
                            },
                            ..
                        } => {
                            // we check if we know this peer, because if it is an authority
                            // we can upgrade the location information
                            if let Some(pi) = self.cache.retrieve::<PeerInfo>(peer_id.to_string().as_str()).await? {
                                debug!("Connection established to authority: {} on {address}", pi.authority);
                                connected_peer_addr.insert(peer_id,address);
                            }
                            else {
                                debug!("Connection established to peer: {peer_id} on {address}");
                            }
                        },
                        // Connection related events, sometimes peers don't allow connection to us, but the network
                        // is telling us the address they are at
                        SwarmEvent::OutgoingConnectionError {
                            connection_id: _ci,
                            peer_id,
                            error
                        } => debug!("Outgoing connection to peer {peer_id:?} failed with error {error:?}"),

                        SwarmEvent::ConnectionClosed {
                            connection_id,
                            cause: Some(error),
                            ..
                        } => debug!("ConnectionId {connection_id:?} closed cause {error:?}"),


                        // Behaviour Events
                        SwarmEvent::Behaviour(behavior_event) => {
                            match behavior_event {
                                BehaviourEvent::Discovery(KademliaEvent::OutboundQueryProgressed {
                                    id,
                                    result: QueryResult::GetRecord(record),
                                    ..
                                }) => {
                                    // Has received at least one answer for this and can advance the queries.
                                    match record {
                                        Ok(GetRecordOk::FoundRecord(peer_record)) => {
                                            // Check that the task has not already been closed
                                            if let Some(TaskExecution{task,..}) = task_execs.get(&id) {
                                                let key = discover_keys.get(&id).expect("Fatal Error, missing key");
                                                let Task::Discover(authority) = task else {panic!("Fatal Error, invalid discovery task")};

                                                let _found_key = peer_record.record.key;
                                                let found_value = peer_record.record.value;

                                                // If the record is authentic we must be able to decode it
                                                // with the key of the authority
                                                match decode_dht_record(found_value, &key).map_err(|e| TopError::NetworkError(format!("Failed to decode DHT record: {e:?}"))) {
                                                    Ok((peer_id, addresses)) => {
                                                        debug!("Discovery /Decoding DHT worked for authority {authority}");
                                                        let ips = self.resolver.resolve(&addresses).await?;
                                                        // Update the authority information
                                                        let al = AuthorityLocation::Declared{
                                                            listening_addresses: addresses.clone(),
                                                            time_stamp: Utc::now(),
                                                            ips: self.resolver.resolve(&addresses).await?,
                                                            geo_locations: self.geo_ip.lookup_ips(&ips).await?,
                                                        };
                                                        let mut ai = match self.cache.retrieve::<AuthorityInfo>(authority.to_hex().as_str()).await? {
                                                            Some(ai)=>ai,
                                                            None =>{
                                                                let ai = AuthorityInfo::new(authority.clone());
                                                                ai
                                                            }};
                                                        ai.best_location = al;
                                                        ai.peer_id=Some(peer_id.to_string());
                                                        // We store the info about the authority
                                                        self.cache.store(&ai).await?;
                                                        // We now know which PeerId is the Authority
                                                        self.cache.store(&PeerInfo::new(peer_id.to_string(),authority.clone())).await?;
                                                        info!("Discovered location for authority: {authority}");
                                                        task_execs.remove(&id);
                                                        // We check if we are done, or not yet...
                                                        if task_execs.is_empty() {
                                                            info!("Completed all network tasks!");
                                                            break;
                                                        }
                                                    },
                                                    Err(e) => {
                                                        debug!(
                                                            "Decoding DHT failed for authority {:?}: {:?}",
                                                            authority, e
                                                        );
                                                    }
                                                }
                                            }
                                            else {
                                                debug!("No task record for query {id:?}, already finalized ?");
                                            };
                                        },
                                        _ => (),
                                    }
                                },
                                BehaviourEvent::Discovery(KademliaEvent::OutboundQueryProgressed {
                                    id,
                                    result: QueryResult::GetClosestPeers(r),
                                    ..
                                }) => {
                                    println!("Query Progress {:?} {:?}", id, r)
                                },
                                // A peer has been identified, we managed to establish a connection
                                // Note: Observed Addr does not give us what we want.
                                // We get the actual connected address from the connection established event
                                BehaviourEvent::PeerInfo(
                                    Identified { peer_id, ..}
                                )=> {
                                    // we check if we know this peer, because if it is an authority
                                    // we can upgrade the location information
                                    if let Some(pi) = self.cache.retrieve::<PeerInfo>(peer_id.to_string().as_str()).await? {
                                        // retrieve the authority info
                                        if let Some(mut ai) = self.cache.retrieve::<AuthorityInfo>(pi.authority.to_hex().as_str()).await? {
                                            if let Some(addr) = connected_peer_addr.get(&peer_id) {
                                                let ips =  self.resolver.resolve(&vec![addr.clone()]).await?;
                                                if ips.len() > 0 {
                                                    let geo_locations = self.geo_ip.lookup_ips(&ips).await?;
                                                    if geo_locations.len()>1{
                                                        // We don't really care if there are multiple IPs as long as they are on the same location.
                                                        // but we only support one location for an identified authority
                                                        // there can be many if the connected address is DNS and it returns multiple A records.
                                                        warn!("Identification results in multiple geolocations, discarding rest {ips:?}");
                                                    }
                                                    let ip = ips[0];
                                                    let al = AuthorityLocation::Identified{
                                                        address: addr.clone(),
                                                        time_stamp: Utc::now(),
                                                        ip,
                                                        geo_location: self.geo_ip.lookup(ip).await?,
                                                    };
                                                    let old_location = ai.best_location;
                                                    ai.best_location = al;
                                                    self.cache.store(&ai).await?;
                                                    // what has actually changed?
                                                    if let AuthorityLocation::Identified{
                                                        address: oa,
                                                        time_stamp: ot,
                                                        ..
                                                    } = old_location {
                                                        match (oa,ot){
                                                            (a,_) if a != *addr => {
                                                                info!("Identified authority {} , address changed", pi.authority);
                                                            },
                                                            (_,t) if t < Utc::now() + Duration::from_secs( 60u64 *60u64) => {
                                                                // there are many of these, and are not so interesting
                                                                debug!("Identified authority {} , updated last seen timestamp", pi.authority);
                                                            }
                                                        _ => {}}
                                                    }
                                                    else {
                                                        info!("Identified authority {}", pi.authority);
                                                    }
                                                }
                                                else {
                                                    warn!("Could not resolve any IP for Authority {} that connected to us via {}", pi.authority, addr);
                                                }
                                            }
                                            else {
                                                warn!("Authority {} was identified but no connection established event recorded its address", pi.authority);
                                            }
                                        }
                                        else {
                                            warn!("Found PeerInfo but no AuthorityInfo for {}", pi.peer_id);
                                        }
                                    }
                                    else {
                                        debug!("Peer Identified {peer_id:?}");
                                    }

                                },
                                _ => debug!("Unknown Behaviour event received {behavior_event:?}"),
                            }
                        },
                        // An event that we are not dealing with
                        _ => debug!("Unknown SwarmEvent received {event:?}")
                    }
                },
                // TODO: introduce random walks when the network stops generating events, not sure if
                // they are required always, but the time wihtout events prior to introducing the walk could be configured
                _ = sleep(Duration::from_secs(self.cfg.p2p_exec_time_s as u64)) => {
                    warn!("Network Process did Timeout with {} pending tasks that will be postponed", task_execs.len());
                    // return the failed tasks to the queue, increasing the failures
                    for (_,te) in task_execs.iter_mut(){
                        te.retries+=1;
                        self.cache.enqueue(&te.clone()).await?;
                    }
                    break;
                }
                _ = shutdown.recv() => {
                    warn!("Shutdown received, postponing {} pending tasks...",task_execs.len());
                    // we just leave them back in the queue
                    for (_,te) in task_execs.iter(){
                        self.cache.enqueue(&te.clone()).await?;
                    }
                    break;
                }
            }
        }
        Ok(())
    }

    // Checks the network tasks and if there are pending tasks
    // starts its processing
    async fn check_network_tasks(&self) -> Result<(), TopError> {
        let tasks_execs = self.cache.queue_length::<TaskExecution>().await?;
        if tasks_execs > 0 {
            info!("{tasks_execs} network tasks in queue...");
            self.process_network_tasks().await?;
        } else {
            info!("No pending tasks");
        }
        Ok(())
    }

    pub async fn wait_for_tasks(&self) -> Result<(), Box<dyn Error>> {
        // Create a shutdown receiver

        let mut shutdown = self.shutdown_tx.subscribe();

        // Crate job intervals
        let mut network_tasks_interval = interval(Duration::from_secs(
            self.cfg.p2p_task_launch_int_m as u64 * 60u64,
        ));

        // Wait for tasks until shutdown
        loop {
            tokio::select! {
               _ = network_tasks_interval.tick() => {
                    let r = self.check_network_tasks().await;
                    if let Err(e) = r {
                        warn!("Network task processing failed with: {e}",);
                    }
                }
                _ = shutdown.recv() => {
                    warn!("p2p is shutting down...");
                    break;
                }
            }
        }
        Ok({})
    }
}

#[cfg(test)]
mod test {
    use libp2p::Multiaddr;

    static TEST_ADDR: &str = "/dns/westend-bootnode-0.polkadot.io/tcp/30333/p2p/12D3KooWKer94o1REDPtAhjtYR4SdLehnSrN8PEhBnZm5NBoCrMC";

    #[test]
    fn can_parse_multiaddress() {
        let ma: Multiaddr = TEST_ADDR.parse().expect("Invalid Multiaddress");
        if let Some(c) = ma.iter().next() {
            assert_eq!(c, c);
        }
    }
}
