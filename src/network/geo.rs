// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

// This product includes GeoLite2 data created by MaxMind, available from www.maxmind.com

use crate::cache::Cache;
use crate::cli::Args;
use crate::types::{
    ChainInfo, CountryLocation, CountryLocationType, GeoIpLocation, GeoIpLocationLookup,
    NetworkLocation, TopError,
};
use jsonrpsee::core::Serialize;
use maxminddb::geoip2::model::RepresentedCountry;
use maxminddb::geoip2::{model, Asn, Country};
use maxminddb::{geoip2, MaxMindDBError, Reader};
use rocket::form::validate::Contains;
use rocket::serde::Deserialize;
use std::net::IpAddr;
use std::ops::Deref;
use std::sync::{Arc, Mutex};

type DbFile = maxminddb::Reader<Vec<u8>>;
static ENGLISH: &str = "en";

static ASN_GROUPS: [&str; 7] = [
    "GOOGLE",
    "AMAZON",
    "OVH",
    "MICROSOFT",
    "DIGITALOCEAN",
    "ALIBABA",
    "HETZNER",
];

// TODO: we need to detect when the database file has changed and trigger a reload
// The database is entirely hold in memory.

pub struct GeoIP {
    cfg: Arc<Args>,
    country_db: Arc<Mutex<Option<DbFile>>>,
    asn_db: Arc<Mutex<Option<DbFile>>>,
    cache: Cache,
    chain_info: Arc<ChainInfo>,
}

impl GeoIP {
    pub async fn new(cfg: Arc<Args>, chain_info: Arc<ChainInfo>) -> Result<Self, TopError> {
        let asn_db = Reader::open_readfile(format!("{}/{}", cfg.geoip_dir, "GeoLite2-ASN.mmdb"))?;
        let country_db =
            Reader::open_readfile(format!("{}/{}", cfg.geoip_dir, "GeoLite2-Country.mmdb"))?;

        let cache = Cache::new(cfg.clone(), chain_info.clone()).await?;

        Ok(GeoIP {
            cfg,
            country_db: Arc::new(Mutex::new(Some(country_db))),
            asn_db: Arc::new(Mutex::new(Some(asn_db))),
            cache,
            chain_info,
        })
    }

    // Can we use the premoum web service?`
    pub fn has_ws_lookup(&self) -> bool {
        match (self.cfg.geoip_id.clone(), self.cfg.geoip_key.clone()) {
            (Some(_), Some(_)) => true,
            _ => false,
        }
    }

    // If we have premium capability, we search with it
    // If we don't have it or it fails, we return the DB lookup
    pub async fn lookup(&self, ip: IpAddr) -> Result<GeoIpLocation, TopError> {
        match self.has_ws_lookup() {
            true => {
                match self
                    .cache
                    .retrieve::<GeoIpLocationLookup>(ip.to_string().as_str())
                    .await
                {
                    // If we have this lookup in cache...
                    Ok(Some(ipl)) => Ok(ipl.location),
                    // else we look it up
                    _ => match self.ws_lookup(ip).await {
                        Ok(loc) => {
                            // We cache this for next time
                            _ = self
                                .cache
                                .store_ttl(
                                    &GeoIpLocationLookup {
                                        ip,
                                        location: loc.clone(),
                                    },
                                    self.cfg.geoip_cache_expiry,
                                )
                                .await;
                            Ok(loc)
                        }
                        Err(_) => self.db_lookup(ip),
                    },
                }
            }
            false => self.db_lookup(ip),
        }
    }

    // IP Looup based on premium ws
    pub(crate) async fn ws_lookup(&self, ip: IpAddr) -> Result<GeoIpLocation, TopError> {
        if self.has_ws_lookup() {
            let client = reqwest::Client::new();
            let location = client
                .get(format!(
                    "https://geoip.maxmind.com/geoip/v2.1/insights/{}",
                    ip
                ))
                .basic_auth(
                    self.cfg.geoip_id.clone().unwrap(),
                    Some(self.cfg.geoip_key.clone().unwrap()),
                )
                .send()
                .await?
                .text()
                .await?;

            let insights: Insights = serde_json::from_str(&location)?;
            let network = insights.network.clone().and_then(|asn| Some(asn.into()));
            let country = CountryLocation::try_from(Country::from(insights)).ok();

            Ok(GeoIpLocation { network, country })
        } else {
            Err(TopError::NetworkError(
                "GeoIP AccountId/Key not provided".to_string(),
            ))
        }
    }

    // IP Lookup based on free databases
    pub(crate) fn db_lookup(&self, ip: IpAddr) -> Result<GeoIpLocation, TopError> {
        let binding = self.asn_db.lock().unwrap();
        let asn_db = binding.deref();

        // Get the asn, ensuring that the db is ready, and handling errors / missing info

        let network: Option<NetworkLocation> = match asn_db {
            Some(db) => match db.lookup::<geoip2::Asn>(ip) {
                Ok(asn) => Some(asn.into()),
                Err(MaxMindDBError::AddressNotFoundError(_)) => None,
                Err(e) => Err(e)?,
            },
            None => Err(TopError::NetworkError(
                "GeoIP ASN Database not ready".parse().unwrap(),
            ))?,
        };

        let binding = self.country_db.lock().unwrap();
        let country_db = binding.deref();

        // Get the asn, ensuring that the db is ready, and handling errors / missing info

        let country: Option<CountryLocation> = match country_db {
            Some(db) => match db.lookup::<geoip2::Country>(ip) {
                Ok(country) => CountryLocation::try_from(country).ok(),
                Err(MaxMindDBError::AddressNotFoundError(_)) => None,
                Err(e) => Err(e)?,
            },
            None => Err(TopError::NetworkError(
                "GeoIP Country Database not ready".parse().unwrap(),
            ))?,
        };

        // return both
        Ok(GeoIpLocation { country, network })
    }

    // resolves a set of IPs and reduces them if they are equal
    pub async fn lookup_ips(&self, ips: &Vec<IpAddr>) -> Result<Vec<GeoIpLocation>, TopError> {
        let mut ret = Vec::<GeoIpLocation>::new();
        for ip in ips {
            let l = self.lookup(ip.clone()).await?;
            if !ret.contains(&l) {
                ret.push(l);
            }
        }
        Ok(ret)
    }

    /// We attempt to group well known global providers into one group name
    fn asn_group_name(asn: &Asn) -> Option<String> {
        match asn.autonomous_system_organization {
            Some(asn) => {
                let asn_upper = asn.to_string().to_uppercase();
                Some(
                    ASN_GROUPS
                        .iter()
                        .find(|&&g| asn_upper.contains(g))
                        .unwrap_or_else(|| &asn)
                        .to_string(),
                )
            }
            None => None,
        }
    }

    /// Returns the best option for country from the available ones.
    fn best_country_code(country: &Country) -> Option<(CountryLocationType, String, String)> {
        // We assume that if the iso_code is present, the name is too (how hard could it be).
        // We set the following priority: represented, country, registered.
        // Represented country is assumed for legally overseas territories, such as embassies
        // country is the one likely to be physical location
        // registered, does not tell us much, but we use it as a fall back, in case nothing else is present.
        match (
            &country.represented_country,
            &country.country,
            &country.registered_country,
        ) {
            (
                Some(RepresentedCountry {
                    iso_code: Some(iso_code),
                    names: Some(names),
                    ..
                }),
                _,
                _,
            ) => Some((
                CountryLocationType::Represented,
                iso_code.to_string(),
                names.get(ENGLISH).unwrap().to_string(),
            )),
            (
                _,
                Some(model::Country {
                    iso_code: Some(iso_code),
                    names: Some(names),
                    ..
                }),
                _,
            ) => Some((
                CountryLocationType::Physical,
                iso_code.to_string(),
                names.get(ENGLISH).unwrap().to_string(),
            )),
            (
                _,
                _,
                Some(model::Country {
                    iso_code: Some(iso_code),
                    names: Some(names),
                    ..
                }),
            ) => Some((
                CountryLocationType::Registered,
                iso_code.to_string(),
                names.get(ENGLISH).unwrap().to_string(),
            )),
            _ => None,
        }
    }
}

// The insignts data structure can be composed from
// types already available on the DB version
// there is more data avialble but we pick what we need.
#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Insights<'a> {
    #[serde(borrow)]
    pub continent: Option<model::Continent<'a>>,
    pub country: Option<model::Country<'a>>,
    pub city: Option<model::City<'a>>,
    pub location: Option<model::Location<'a>>,
    pub registered_country: Option<model::Country<'a>>,
    pub represented_country: Option<model::RepresentedCountry<'a>>,
    pub postal: Option<model::Postal<'a>>,
    #[serde(rename = "traits")]
    pub network: Option<Asn<'a>>,
}

impl<'a> From<Insights<'a>> for Country<'a> {
    fn from(value: Insights<'a>) -> Self {
        Country {
            continent: value.continent,
            country: value.country,
            registered_country: value.registered_country,
            represented_country: value.represented_country,
            traits: None,
        }
    }
}

impl<'a> From<Insights<'a>> for Asn<'a> {
    fn from(value: Insights<'a>) -> Self {
        Asn {
            autonomous_system_number: value
                .network
                .clone()
                .and_then(|n| n.autonomous_system_number),
            autonomous_system_organization: value
                .network
                .and_then(|n| n.autonomous_system_organization),
        }
    }
}

impl<'a> From<Asn<'a>> for NetworkLocation {
    fn from(asn: Asn<'a>) -> Self {
        NetworkLocation {
            asn_group_code: None,
            asn_group_name: GeoIP::asn_group_name(&asn),
            asn_code: asn
                .autonomous_system_number
                .and_then(|n| Some(n.to_string())),
            asn_name: asn
                .autonomous_system_organization
                .and_then(|n| Some(n.to_string())),
        }
    }
}

pub enum GeoConversionError {
    MissingData,
}

impl<'a> TryFrom<Country<'a>> for CountryLocation {
    type Error = GeoConversionError;

    fn try_from(country: Country<'a>) -> Result<Self, Self::Error> {
        // There needs to be at least one of the options
        match GeoIP::best_country_code(&country) {
            Some((tpe, iso, name)) => Ok(CountryLocation {
                location_type: tpe,
                country_code: Some(iso),
                country_name: Some(name),
                group_code: country
                    .continent
                    .clone()
                    .and_then(|c| c.code)
                    .and_then(|c| Some(c.to_string())),
                group_name: country
                    .continent
                    .and_then(|c| c.names)
                    .and_then(|c| Some(c.get(ENGLISH).unwrap().to_string())),
            }),
            None => Err(GeoConversionError::MissingData),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::cli::Args;
    use crate::network::geo::GeoIP;
    use crate::types::{BlockAuthoring, ChainInfo, TopError};
    use clap::Parser;
    use maxminddb::geoip2::Asn;
    use std::net::IpAddr;
    use std::sync::Arc;

    pub fn default_cfg() -> Arc<Args> {
        Arc::new(Args::parse_from(vec!["toporacle", "-G", "./src/test/data"]))
    }

    pub async fn create_geo_ip() -> GeoIP {
        let cfg = default_cfg();
        let chain_info = Arc::new(ChainInfo {
            name: "Westend".to_string(),
            code: "westend".to_string(),
            ss58_format: 42,
            genesis: "0x0".to_string(),
            boot_nodes: vec![],
            block_authoring: BlockAuthoring::BABE,
        });
        GeoIP::new(cfg, chain_info).await.unwrap()
    }

    #[tokio::test]
    async fn will_check_well_known_ips() {
        let gi = create_geo_ip().await;
        let ip: IpAddr = "1.1.1.1".parse().unwrap();
        let l = gi.db_lookup(ip).unwrap();

        assert_eq!(l.country.unwrap().country_code.unwrap(), "AU");

        let ip: IpAddr = "81.236.63.162".parse().unwrap();
        let l = gi.db_lookup(ip).unwrap();
        assert_eq!(l.country.unwrap().country_code.unwrap(), "SE");
    }

    #[tokio::test]
    async fn will_test_asn_grouping() {
        let a = Asn {
            autonomous_system_number: None,
            autonomous_system_organization: Some("Google Private Data Blackhole Inc."),
        };
        let g = GeoIP::asn_group_name(&a);
        assert_eq!(g, Some("GOOGLE".to_string()));

        let a = Asn {
            autonomous_system_number: None,
            autonomous_system_organization: Some("Microsoft Mass Surveillance Inc."),
        };
        let g = GeoIP::asn_group_name(&a);
        assert_eq!(g, Some("MICROSOFT".to_string()));

        let a = Asn {
            autonomous_system_number: None,
            autonomous_system_organization: Some("Small Provider Inc."),
        };
        let g = GeoIP::asn_group_name(&a);
        assert_eq!(g, Some("Small Provider Inc.".to_string()));
    }

    #[tokio::test]
    async fn will_test_geolocation_equality() {
        let gi = create_geo_ip().await;
        let ip: IpAddr = "1.1.1.1".parse().unwrap();
        let l1 = gi.db_lookup(ip).unwrap();
        let l2 = gi.db_lookup(ip).unwrap();
        assert_eq!(l1, l2);
    }

    // TODO: requires environment variables for license keys
    #[tokio::test]
    async fn will_test_premium_ws_geolocation() {
        let gi = create_geo_ip().await;

        let ip: IpAddr = "81.236.63.162".parse().unwrap();
        match gi.ws_lookup(ip).await {
            Ok(l1) => assert_eq!(l1.country.unwrap().country_code.unwrap(), "SE".to_string()),
            // We ignore the test when license key was not found in the environment
            Err(TopError::NetworkError(msg)) => assert!(msg.contains("AccountId/Key")),
            Err(_) => panic!("Premium location failed"),
        }
    }
}
