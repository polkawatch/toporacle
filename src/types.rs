// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use crate::cache::{CacheKey, CacheQueueCode, CacheQueueEntry, Cacheable};
use async_std_resolver::ResolveError;
use chrono::{DateTime, Utc};
use hex::FromHexError;
use jsonrpsee::core::Serialize;
use libp2p::core::transport::TransportError;
use libp2p::multiaddr::Error as MultiaddrError;
use libp2p::Multiaddr;
use maxminddb::MaxMindDBError;
use parity_scale_codec::Error as ParityCodecError;
use redis::RedisError;
use reqwest::Error as ReqwestError;
use rocket::serde::Deserialize;
use rocket_okapi::JsonSchema;
use serde::de::Error as DeError;
use serde::{Deserializer, Serializer};
use std::error::Error;
use std::fmt;
use std::fmt::{Display, Formatter};
use std::net::IpAddr;
use std::str::FromStr;
use subp2p_explorer::util::crypto::sr25519;
use subp2p_explorer::util::crypto::sr25519::PublicKey;
use subp2p_explorer::util::ss58::to_ss58;

#[derive(Debug)]
#[allow(unused)]
pub enum TopError {
    // Own Errors
    ApiError(String),
    ChainError(String),
    NetworkError(String),
    // Dependency wrapped
    GeoIPError(MaxMindDBError),
    ResolveError(ResolveError),
    MultiaddrError(MultiaddrError),
    TransportError(TransportError<std::io::Error>),
    CacheError(RedisError),
    SerdeError(String),
    HexError(FromHexError),
    InvalidHexPrefix,
    InvalidDataLength,
    SubstrateError(subxt::Error),
    ParityCodecError(ParityCodecError),
    ReqwestError(ReqwestError),
    SerdeJsonError(serde_json::Error),
}

impl Display for TopError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TopError::ApiError(e) => write!(f, "Api Error: {}", e),
            TopError::ChainError(e) => write!(f, "Chain Error: {}", e),
            TopError::NetworkError(e) => write!(f, "Network Error: {}", e),
            TopError::CacheError(e) => write!(f, "Cache / Redis Error: {}", e),
            TopError::SerdeError(e) => write!(f, "Serde Error: {e}"),
            TopError::InvalidHexPrefix => write!(f, "must be prefixed by 0x"),
            TopError::InvalidDataLength => write!(f, "must be 32 bytes hex"),
            TopError::HexError(msg) => write!(f, "Hex Error: {}", msg),
            TopError::SubstrateError(e) => write!(f, "Serde Error: {e}"),
            TopError::ParityCodecError(e) => write!(f, "Serde Error: {e}"),
            TopError::MultiaddrError(e) => write!(f, "P2P Multiaddr Error: {e}"),
            TopError::TransportError(e) => write!(f, "P2P Transport Error: {e}"),
            TopError::ResolveError(e) => write!(f, "Resolver Error: {e}"),
            TopError::GeoIPError(e) => write!(f, "GeoIP Error: {e}"),
            TopError::ReqwestError(e) => write!(f, "Reqwest Error: {e}"),
            TopError::SerdeJsonError(e) => write!(f, "Serde JSON Error: {e}"),
        }
    }
}

impl From<serde_json::Error> for TopError {
    fn from(value: serde_json::Error) -> Self {
        TopError::SerdeJsonError(value)
    }
}

impl From<ReqwestError> for TopError {
    fn from(value: ReqwestError) -> Self {
        TopError::ReqwestError(value)
    }
}

impl From<MaxMindDBError> for TopError {
    fn from(value: MaxMindDBError) -> Self {
        TopError::GeoIPError(value)
    }
}

impl From<ResolveError> for TopError {
    fn from(value: ResolveError) -> Self {
        TopError::ResolveError(value)
    }
}

impl From<RedisError> for TopError {
    fn from(e: RedisError) -> Self {
        TopError::CacheError(e)
    }
}

impl From<FromHexError> for TopError {
    fn from(value: FromHexError) -> Self {
        TopError::HexError(value)
    }
}

impl From<subxt::Error> for TopError {
    fn from(value: subxt::Error) -> Self {
        TopError::SubstrateError(value)
    }
}

impl From<ParityCodecError> for TopError {
    fn from(value: ParityCodecError) -> Self {
        TopError::ParityCodecError(value)
    }
}

impl From<MultiaddrError> for TopError {
    fn from(value: MultiaddrError) -> Self {
        TopError::MultiaddrError(value)
    }
}

impl From<TransportError<std::io::Error>> for TopError {
    fn from(value: TransportError<std::io::Error>) -> Self {
        TopError::TransportError(value)
    }
}

impl Error for TopError {}

/// An Authority, that we can serialize as hex encoded
#[derive(Debug, PartialEq, Clone, JsonSchema)]
pub struct Authority(sr25519::PublicKey);

/// A marker type, documenting how we plan to store PeerIds, as String encoded Public Keys
/// just like p2p does.
pub type P2PPublicKeyStr = String;

/// Marker type, documenting the way to store Authority Key Owners.
pub type OwnerSS58Str = String;

impl Authority {
    pub fn to_ss58(&self, version: u16) -> String {
        to_ss58(&self.0, version)
    }
    pub fn to_hex(&self) -> String {
        format!("0x{}", hex::encode(self.0))
    }
}

impl FromStr for Authority {
    type Err = TopError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // must have its header
        if !(s.starts_with("0x")) {
            return Err(TopError::InvalidHexPrefix);
        };
        // must be valid hex, and have the right length to be a PublicKey
        let pkey: sr25519::PublicKey = hex::decode(&s[2..])?
            .try_into()
            .map_err(|_| TopError::InvalidDataLength)?;
        Ok(Authority(pkey))
    }
}

impl From<sr25519::PublicKey> for Authority {
    fn from(key: PublicKey) -> Self {
        Authority(key)
    }
}

impl Into<sr25519::PublicKey> for Authority {
    fn into(self) -> PublicKey {
        self.0
    }
}

impl fmt::Display for Authority {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_hex())
    }
}

impl Serialize for Authority {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&*self.to_hex())
    }
}

impl<'de> Deserialize<'de> for Authority {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let hex = String::deserialize(deserializer)?;
        Ok(hex
            .as_str()
            .parse()
            .map_err(|e: TopError| DeError::custom(e.to_string()))?)
    }
}

/// How to store an Authority in Redis Cache
impl CacheKey for Authority {
    fn cache_key(&self) -> String {
        self.to_hex()
    }

    fn cache_type_code() -> String {
        "authority".to_string()
    }
}

impl Cacheable for Authority {}

/// The Network Location for an Authority IP
#[derive(Debug, Deserialize, Serialize, JsonSchema, Clone, PartialEq)]
pub struct NetworkLocation {
    /// The ASN group code, used to group ogranizations with multiple ASNs (i.e. Google).
    pub asn_group_code: Option<String>,
    /// The ASN group name
    pub asn_group_name: Option<String>,
    /// The Autonomous System Number
    pub asn_code: Option<String>,
    /// The Autonomous System Organization
    pub asn_name: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, JsonSchema, Clone, PartialEq)]
pub enum CountryLocationType {
    /// The physical location
    Physical,
    /// The territory being represented at the location, typically used for oversea territories such as embasies.
    Represented,
    /// Where the IP was registered
    Registered,
}

/// The resolved Country Location for an authority IP
#[derive(Debug, Deserialize, Serialize, JsonSchema, Clone, PartialEq)]
pub struct CountryLocation {
    /// The source of the location information
    pub location_type: CountryLocationType,
    /// The continent or group code of a country
    pub group_code: Option<String>,
    /// The continent or group name of a country
    pub group_name: Option<String>,
    /// the ISO code of the country
    pub country_code: Option<String>,
    /// the name of the country
    pub country_name: Option<String>,
}

/// GeoIP Information for an Authority, derived form its network address
#[derive(Debug, Deserialize, Serialize, JsonSchema, Clone, PartialEq)]
pub struct GeoIpLocation {
    pub country: Option<CountryLocation>,
    pub network: Option<NetworkLocation>,
}

// We cache premium lookups, as they are paid per request
#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
pub struct GeoIpLocationLookup {
    pub ip: IpAddr,
    pub location: GeoIpLocation,
}

impl CacheKey for GeoIpLocationLookup {
    fn cache_key(&self) -> String {
        self.ip.to_string()
    }

    fn cache_type_code() -> String {
        "ip-lookup".to_string()
    }
}

impl Cacheable for GeoIpLocationLookup {}

/// Authority Location Record depending on the level of discovery achieved.
#[derive(Debug, Deserialize, Serialize, JsonSchema, PartialEq, Clone)]
#[serde(tag = "discovery_type")]
pub enum AuthorityLocation {
    /// No Discovery or Identification has been achieved yet.
    Unknown {
        /// The timestamp in which the authority key was first found.
        time_stamp: DateTime<Utc>,
    },
    /// The Authority has declared via signed DHT record its addressing. A tuple with a vector of listening addresses, and the timestamp of the last DHT record verification is provided.
    Declared {
        /// The addresses where the authority claims to be listening at.
        #[schemars(with = "Vec<String>")]
        listening_addresses: Vec<Multiaddr>,
        /// The last time we verivied this declaration.
        time_stamp: DateTime<Utc>,
        /// The list of IP addresses this declarations resolve to.
        ips: Vec<IpAddr>,
        /// The List of GeoLocations the IPs resolve to
        geo_locations: Vec<GeoIpLocation>,
    },
    /// We managed to ping and Identify the peer at this specific address. A tuple with the address and last ping timestamp is provided.
    Identified {
        /// The last time the authority was Identified.
        time_stamp: DateTime<Utc>,
        /// The multiaddress where the authority was identified.
        #[schemars(with = "String")]
        address: Multiaddr,
        // TODO: this should actually be just one IP and should be the IP of the effective connection during the identification.
        // DNS recods could resolve to more than one IP
        /// The IP address the multiaddress resolves to.
        ip: IpAddr,
        /// The Geolocation of the IP
        geo_location: GeoIpLocation,
    },
}

/// We will perform specific tasks, minimizing p2p network traffic
/// The Tasks that we need to perform
#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub enum Task {
    /// We need to discover DHT Records for an Authority
    Discover(Authority),
    /// We need to dial/ping and identify a particular peer Id
    Identify(Authority),
    /// We need to refresh the data of an Authority
    /// because a change has been detected, such as change of keys
    Refresh(Authority),
}

/// A Struct to track the execution of tasks
/// They must expire at some point to avoid tasks forever
/// Also they will track how many acc failures there are
#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct TaskExecution {
    pub(crate) task: Task,
    pub(crate) retries: u16,
    pub(crate) expiry: DateTime<Utc>,
}

impl CacheQueueCode for TaskExecution {
    fn cache_queue_code() -> String {
        "task-executions".to_string()
    }
}

/// Tasks can be enqueued
impl CacheQueueEntry for TaskExecution {}

// TODO: What to do with OLD authorities?
/// What we know about an authority discovery key
#[derive(Debug, Serialize, Deserialize, JsonSchema, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct AuthorityInfo {
    /// The ID of the authority as hex string prefixed by 0x
    #[schemars(with = "String")]
    pub id: Authority,
    /// The related PeerID as Libp2p compressed public key string
    pub peer_id: Option<P2PPublicKeyStr>,
    /// The related Key Owner as Chain dependent SS58 string
    #[serde(rename = "ownerSS58")]
    pub owner_ss58: Option<OwnerSS58Str>,
    /// Te best location discovered for the authority.
    pub best_location: AuthorityLocation,
    #[serde(skip)]
    #[schemars(skip)]
    pub location_history: Option<Vec<AuthorityLocation>>,
}

impl AuthorityInfo {
    /// New Authority Identified we know nothing about yet.
    pub(crate) fn new(id: Authority) -> Self {
        AuthorityInfo::new_owned(id, None)
    }

    /// Convenience construction, when we also find out the owner
    pub(crate) fn new_owned(id: Authority, owner_ss58: Option<OwnerSS58Str>) -> Self {
        // We know nothing about this authority, except that was identified as such right now.
        AuthorityInfo {
            id,
            peer_id: None,
            owner_ss58,
            best_location: AuthorityLocation::Unknown {
                time_stamp: Utc::now(),
            },
            location_history: None,
        }
    }
}

/// How to store an Authority Info in Redis Cache
impl CacheKey for AuthorityInfo {
    fn cache_key(&self) -> String {
        self.id.to_hex()
    }

    fn cache_type_code() -> String {
        "authority-info".to_string()
    }
}

impl Cacheable for AuthorityInfo {}

/// Key Owner Info
/// Key Owners are typically the validators, that can update/rotate their keys as required
/// It is important to be able to access the Authority Discovery information by Key Owner,
#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct KeyOwnerInfo {
    /// The SS58 String for this key owner. The typical encoding used in UI.
    pub id_ss58: OwnerSS58Str,
    /// The Authority Location Key
    pub authority: Authority,
    /// Last time we noted this association
    pub last_seen: DateTime<Utc>,
}

impl KeyOwnerInfo {
    pub fn new(id_ss58: OwnerSS58Str, authority: Authority) -> Self {
        KeyOwnerInfo {
            id_ss58,
            authority,
            last_seen: Utc::now(),
        }
    }
}

impl CacheKey for KeyOwnerInfo {
    fn cache_key(&self) -> String {
        self.id_ss58.clone()
    }

    fn cache_type_code() -> String {
        "key-owner-account".to_string()
    }
}
impl Cacheable for KeyOwnerInfo {}

/// We also want to store the relationship between PeerId and Authority Discovery Keys
/// That way when we parocess events we know if they are related to any paritcular authority
/// very easily.
#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct PeerInfo {
    pub(crate) peer_id: P2PPublicKeyStr,
    pub(crate) authority: Authority,
    last_seen: DateTime<Utc>,
}

impl PeerInfo {
    pub fn new(peer_id: P2PPublicKeyStr, authority: Authority) -> Self {
        PeerInfo {
            peer_id,
            authority,
            last_seen: Utc::now(),
        }
    }
}

impl CacheKey for PeerInfo {
    fn cache_key(&self) -> String {
        self.peer_id.clone()
    }

    fn cache_type_code() -> String {
        "peer-info".to_string()
    }
}

impl Cacheable for PeerInfo {}

/// A boot node is simply a Multiaddress entrypoint
pub type Bootnode = Multiaddr;

/// The Bootnodes of a Chain is just a collection of bootnodes.
pub type Bootnodes = Vec<Bootnode>;

/// The Block Authoring Type
#[derive(Serialize, Deserialize, JsonSchema, Clone)]
pub enum BlockAuthoring {
    BABE,
    AURA,
}

/// What we know about the chain we are connected to,
/// retrieved from the chain and spec files.
#[derive(Serialize, Deserialize, JsonSchema, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChainInfo {
    /// The name of the chain as reported
    pub name: String,
    /// An arbitrary chain code derived from its name, used for key prefix, conf files, etc.
    pub code: String,
    /// The SS58 Format of the chain
    pub ss58_format: u16,
    /// The genesis hash as a string
    pub genesis: String,
    #[serde(skip)]
    #[schemars(skip)]
    /// The boot nodes of the p2p network
    pub boot_nodes: Bootnodes,
    /// The detected block authoring tech.
    pub block_authoring: BlockAuthoring,
}

/// The current status of authority location discovery
#[derive(Serialize, Deserialize, JsonSchema, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChainLocationStatus {
    /// Number of authorities in this Chain
    pub authorities: u32,
    /// Number of authorities with an unknown location, yet.
    pub unknown: u32,
    /// Number of authorities whose location was declared by the authority itself using signed DHT record
    pub declared: u32,
    /// Number of authorities whose location was verified though p2p identification
    pub identified: u32,
    /// Number of operations pending in the p2p task queue.
    pub queued_p2p_tasks: u32,
}

impl CacheKey for ChainLocationStatus {
    fn cache_key(&self) -> String {
        "location".to_string()
    }
    fn cache_type_code() -> String {
        "chain-stats".to_string()
    }
}

impl Cacheable for ChainLocationStatus {}

#[cfg(test)]
mod test {
    use super::*;

    static TEST_AUTH_KEY: &str =
        "0x0ec5e1d2d044023009c63659c65a79aaf07ecbf5b9887958243aa873a63e5a1b";

    static TEST_MA: &str =
        "/ip4/167.233.1.4/tcp/30333/p2p/12D3KooWHgngLeMxFYUH7pWCQH3MVuse9CPJS9Uh6SRUmGT7qta3";

    static TEST_PI: &str = "12D3KooWHgngLeMxFYUH7pWCQH3MVuse9CPJS9Uh6SRUmGT7qta3";

    #[test]
    fn authority_can_json_serialize() {
        let a: Authority = TEST_AUTH_KEY.parse().unwrap();
        let r = serde_json::to_string(&a).unwrap();
        let b: Authority = serde_json::from_str(&*r.clone()).unwrap();
        assert_eq!(a, b);
    }

    #[test]
    fn task_can_json_serialize() {
        let a: Authority = TEST_AUTH_KEY.parse().unwrap();
        let t = Task::Discover(a);
        let r = serde_json::to_string(&t).unwrap();
        let t2 = serde_json::from_str(&*r.clone()).unwrap();
        assert_eq!(t, t2);
    }

    #[test]
    fn authority_location_can_json_serialize() {
        let localhost: IpAddr = "127.0.0.1".parse().unwrap();
        let unknown_location = GeoIpLocation {
            country: None,
            network: None,
        };

        let al = AuthorityLocation::Unknown {
            time_stamp: Utc::now(),
        };
        let r = serde_json::to_string(&al).unwrap();
        let bl = serde_json::from_str(&*r).unwrap();
        assert_eq!(al, bl);

        let a: Multiaddr = TEST_MA.parse().unwrap();
        let al = AuthorityLocation::Identified {
            address: a,
            time_stamp: Utc::now(),
            geo_location: unknown_location.clone(),
            ip: localhost.clone(),
        };
        let r = serde_json::to_string(&al).unwrap();
        let bl = serde_json::from_str(&*r).unwrap();
        assert_eq!(al, bl);

        let a: Multiaddr = TEST_MA.parse().unwrap();
        let al = AuthorityLocation::Declared {
            listening_addresses: Vec::from([a]),
            time_stamp: Utc::now(),
            ips: vec![localhost.clone()],
            geo_locations: vec![unknown_location.clone()],
        };
        let r = serde_json::to_string(&al).unwrap();
        let bl = serde_json::from_str(&*r).unwrap();
        assert_eq!(al, bl);
    }

    #[test]
    fn authority_info_can_json_serialize() {
        let a: Multiaddr = TEST_MA.parse().unwrap();
        let localhost: IpAddr = "127.0.0.1".parse().unwrap();
        let unknown_location = GeoIpLocation {
            country: None,
            network: None,
        };

        let mut ai = AuthorityInfo::new(TEST_AUTH_KEY.parse().unwrap());

        ai.peer_id = Some(TEST_PI.parse().unwrap());
        ai.best_location = AuthorityLocation::Identified {
            address: a,
            ip: localhost.clone(),
            time_stamp: Utc::now(),
            geo_location: unknown_location.clone(),
        };
        // This field is not serialized
        ai.location_history = None;

        let r = serde_json::to_string(&ai).unwrap();
        let bi = serde_json::from_str(&*r).unwrap();
        assert_eq!(ai, bi);
    }
}
