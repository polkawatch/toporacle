use std::sync::Arc;

use redis::aio::ConnectionManager;

use serde::{Deserialize, Serialize};
use serde_json;

use crate::cli::Args;
use crate::types::{ChainInfo, TopError};

pub trait CacheKey {
    /// The (primary) key of the entity to cache
    fn cache_key(&self) -> String;

    /// A character representation of the type of key: for example, authority, peerid, etc.
    fn cache_type_code() -> String;
}

pub trait CacheValue: Sized {
    fn cache_value(&self) -> String;
    fn cache_build(s: &str) -> Result<Self, TopError>;
}

/// Entities that can be put in our cache have key and Value
pub trait Cacheable: CacheKey + CacheValue {}

pub trait CacheQueueCode: Sized {
    fn cache_queue_code() -> String;
}

pub trait CacheQueueEntry: CacheQueueCode + CacheValue {}

/// All serializable entities can be cached the same way
impl<T> CacheValue for T
where
    T: Serialize + for<'a> Deserialize<'a>,
{
    fn cache_value(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    fn cache_build(s: &str) -> Result<T, TopError> {
        Ok(serde_json::from_str::<T>(&s).map_err(|e| TopError::SerdeError(e.to_string()))?)
    }
}

/// The redis cache access object
/// Each stack can create its own instance
#[allow(unused)]
pub struct Cache {
    config: Arc<Args>,
    chain_info: Arc<ChainInfo>,
    redis: ConnectionManager,
}

impl Cache {
    pub async fn new(cfg: Arc<Args>, chain_info: Arc<ChainInfo>) -> Result<Self, TopError> {
        let client = redis::Client::open(cfg.redis.clone()).expect("Could not create redis");
        Ok(Cache {
            config: cfg.clone(),
            chain_info: chain_info.clone(),
            redis: client.get_connection_manager().await?,
        })
    }

    fn full_key<T: Cacheable>(&self, o: &T) -> String {
        format!(
            "{}-{}-{}",
            self.chain_info.code,
            T::cache_type_code(),
            o.cache_key()
        )
    }

    fn build_key<T: Cacheable>(&self, k: &str) -> String {
        format!("{}-{}-{}", self.chain_info.code, T::cache_type_code(), k)
    }

    fn build_queue_key<T: CacheQueueEntry>(&self) -> String {
        format!("{}-{}", self.chain_info.code, T::cache_queue_code())
    }

    pub(crate) async fn store<T: Cacheable>(&self, o: &T) -> Result<(), TopError> {
        let mut con = self.redis.clone();
        let _: () = redis::cmd("SET")
            .arg(self.full_key(o))
            .arg(o.cache_value())
            .query_async(&mut con)
            .await?;
        Ok(())
    }

    pub(crate) async fn store_ttl<T: Cacheable>(&self, o: &T, expiry: u32) -> Result<(), TopError> {
        self.store(o).await?;
        let mut con = self.redis.clone();
        let _: () = redis::cmd("EXPIRE")
            .arg(self.full_key(o))
            .arg(expiry)
            .query_async(&mut con)
            .await?;
        Ok(())
    }

    pub(crate) async fn retrieve<T: Cacheable>(&self, key: &str) -> Result<Option<T>, TopError> {
        let mut con = self.redis.clone();
        let value: Option<String> = redis::cmd("GET")
            .arg(self.build_key::<T>(key))
            .query_async(&mut con)
            .await?;

        match value {
            Some(s) => Ok(Some(T::cache_build(&s)?)),
            _ => Ok(None),
        }
    }

    pub(crate) async fn enqueue<T: CacheQueueEntry>(&self, o: &T) -> Result<(), TopError> {
        let mut con = self.redis.clone();
        let _: () = redis::cmd("LPUSH")
            .arg(self.build_queue_key::<T>())
            .arg(o.cache_value())
            .query_async(&mut con)
            .await?;
        Ok(())
    }

    pub(crate) async fn dequeue<T: CacheQueueEntry>(&self) -> Result<Option<T>, TopError> {
        let mut con = self.redis.clone();
        let value: Option<String> = redis::cmd("RPOP")
            .arg(self.build_queue_key::<T>())
            .query_async(&mut con)
            .await?;

        match value {
            Some(s) => Ok(Some(T::cache_build(&s)?)),
            _ => Ok(None),
        }
    }

    // Returns the queue length
    pub(crate) async fn queue_length<T: CacheQueueEntry>(&self) -> Result<u32, TopError> {
        let mut con = self.redis.clone();
        let value: u32 = redis::cmd("LLEN")
            .arg(self.build_queue_key::<T>())
            .query_async(&mut con)
            .await?;

        Ok(value)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::types::{Authority, AuthorityInfo, BlockAuthoring, Task, TaskExecution};

    use chrono::{DateTime, Utc};
    use clap::Parser;
    use libp2p::Multiaddr;
    use redis::{Client, RedisResult};
    use std::time::Duration;
    use tokio::time::sleep;

    // Our test environment, holds a redis connection and cleans up after done
    // Each tests environment reserves an unique redis key for testing
    #[allow(unused)]
    struct RedisTestEnv {
        key: String,
        cfg: Arc<Args>,
        redis: Client,
    }

    impl RedisTestEnv {
        fn new(key: String) -> Self {
            let cfg = Arc::new(Args::parse_from(vec!["toporacle", "-d", "./src/test/data"]));
            let redis = redis::Client::open(cfg.redis.clone()).expect("Could not create redis");
            RedisTestEnv { key, cfg, redis }
        }
    }

    // this method is, unfortunately, synchronous.
    impl Drop for RedisTestEnv {
        fn drop(&mut self) {
            let mut con = self
                .redis
                .get_connection()
                .expect("Could not get connection");
            let _: () = redis::cmd("DEL")
                .arg(&self.key)
                .query(&mut con)
                .expect("Could not clean up");
        }
    }

    struct CacheTestEnv {
        cache: Cache,
    }

    impl CacheTestEnv {
        async fn new() -> Self {
            let cfg = Arc::new(Args::parse_from(vec!["toporacle", "-d", "./src/test/data"]));
            let chain_info = Arc::new(ChainInfo {
                name: "Westend".to_string(),
                code: "westend".to_string(),
                ss58_format: 42,
                genesis: "0x0".to_string(),
                boot_nodes: vec![],
                block_authoring: BlockAuthoring::BABE,
            });
            CacheTestEnv {
                cache: Cache::new(cfg, chain_info).await.unwrap(),
            }
        }
    }

    static TEST_AUTH_KEY: &str =
        "0x0ec5e1d2d044023009c63659c65a79aaf07ecbf5b9887958243aa873a63e5a1b";

    static TEST_UNKNOWN_AUTH_KEY: &str =
        "0x0000000000000000000000000000000000000000000000000000000000000000";

    static TEST_MA: &str =
        "/ip4/167.233.1.4/tcp/30333/p2p/12D3KooWHgngLeMxFYUH7pWCQH3MVuse9CPJS9Uh6SRUmGT7qta3";

    static TEST_PI: &str = "12D3KooWHgngLeMxFYUH7pWCQH3MVuse9CPJS9Uh6SRUmGT7qta3";

    // Here we are going to test the basic functionality we need form Redis

    #[tokio::test]
    async fn can_set_simple_key_redis() {
        let env = RedisTestEnv::new("test_key".into());
        let mut con = env
            .redis
            .get_async_connection()
            .await
            .expect("Could not get connection");

        let _: () = redis::cmd("SET")
            .arg("test_key")
            .arg(42i32)
            .query_async(&mut con)
            .await
            .expect("Could not Set Value");

        let result: i32 = redis::cmd("GET")
            .arg("test_key")
            .query_async(&mut con)
            .await
            .expect("Could not get Value");

        assert_eq!(result, 42i32);
    }

    #[tokio::test]
    async fn can_set_ttl_simple_key_redis() {
        let env = RedisTestEnv::new("test_key2".into());
        let mut con = env
            .redis
            .get_async_connection()
            .await
            .expect("Could not get connection");

        let _: () = redis::cmd("SET")
            .arg("test_key2")
            .arg(42i32)
            .query_async(&mut con)
            .await
            .expect("Could not Set Value");

        let result: i32 = redis::cmd("GET")
            .arg("test_key2")
            .query_async(&mut con)
            .await
            .expect("Could not get Value");

        assert_eq!(result, 42i32);
    }

    #[tokio::test]
    async fn can_set_pipeline_ttl() {
        let env = RedisTestEnv::new("test_key3".into());
        let mut con = env
            .redis
            .get_async_connection()
            .await
            .expect("Could not get connection");

        let _: () = redis::pipe()
            .cmd("SET")
            .arg("test_key3")
            .arg(42i32)
            .ignore()
            .cmd("EXPIRE")
            .arg("test_key3")
            .arg("3")
            .ignore()
            .query_async(&mut con)
            .await
            .expect("Could not Set-TTL value");

        // I can get the key

        let _result: i32 = redis::cmd("GET")
            .arg("test_key3")
            .query_async(&mut con)
            .await
            .expect("Could not get Value");

        //  At this point an attempt to get would fail

        sleep(Duration::from_secs(4)).await;

        let result: RedisResult<i32> = redis::cmd("GET")
            .arg("test_key3")
            .query_async(&mut con)
            .await;

        assert!(result.is_err());

        // It is also possible to test for existance

        let result: i32 = redis::cmd("EXISTS")
            .arg("test_key3")
            .query_async(&mut con)
            .await
            .expect("Could not check");

        assert_eq!(result, 0);
    }

    #[tokio::test]
    async fn can_set_structured_types() {
        #[derive(PartialEq, Serialize, Deserialize, Debug)]
        enum Class {
            V1,
            V2(String),
            V3(u32),
        }

        #[derive(Serialize, Deserialize, Debug)]
        struct Sample {
            id: u32,
            name: String,
            value: Class,
            time: DateTime<Utc>,
        }

        let env = RedisTestEnv::new("test_key4".into());
        let mut con = env
            .redis
            .get_async_connection()
            .await
            .expect("Could not get connection");

        let sample = Sample {
            id: 100,
            name: "sample name".into(),
            value: Class::V2("hello".into()),
            time: Utc::now(),
        };

        let serialized_sample = serde_json::to_string(&sample).expect("could not serialize");

        let _: () = redis::cmd("SET")
            .arg("test_key4")
            .arg(serialized_sample)
            .query_async(&mut con)
            .await
            .expect("Could not Set Value");

        let result: String = redis::cmd("GET")
            .arg("test_key4")
            .query_async(&mut con)
            .await
            .expect("Could not get Value");

        let sample2: Sample = serde_json::from_str(&result).expect("could not deserialize");

        assert_eq!(sample2.id, 100);
        assert_eq!(sample2.value, Class::V2("hello".into()));
    }

    #[tokio::test]
    async fn can_queue_values() {
        let env = RedisTestEnv::new("test_key5".into());
        let mut con = env
            .redis
            .get_async_connection()
            .await
            .expect("Could not get connection");

        // queue three values
        for v in 1..=3 {
            let _: () = redis::cmd("RPUSH")
                .arg("test_key5")
                .arg(v)
                .query_async(&mut con)
                .await
                .expect("Could not Queue Value");
        }

        // pop them back
        for v in 1..=3 {
            let r: i32 = redis::cmd("LPOP")
                .arg("test_key5")
                .query_async(&mut con)
                .await
                .expect("Could not POP Value");

            assert_eq!(v, r);
        }
    }

    // Now we are going to test the caching

    #[tokio::test]
    async fn can_cache_authority() {
        let env = CacheTestEnv::new().await;

        // Storing an Authority
        let a: Authority = TEST_AUTH_KEY.parse().unwrap();
        let _r = env.cache.store(&a).await.unwrap();

        // Retrieve the Authority, with type inference
        let b = env.cache.retrieve(TEST_AUTH_KEY).await.unwrap().unwrap();
        assert_eq!(a, b);

        // Equivalent when the type cannot be inferred
        let c = env
            .cache
            .retrieve::<Authority>(TEST_AUTH_KEY)
            .await
            .unwrap()
            .unwrap();
        assert_eq!(a, c);

        // What do we get when the authority is not found
        let d = env
            .cache
            .retrieve::<Authority>(TEST_UNKNOWN_AUTH_KEY)
            .await
            .unwrap();
        assert!(d.is_none())
    }

    #[tokio::test]
    async fn can_cache_authority_info() {
        let env = CacheTestEnv::new().await;

        let _a: Multiaddr = TEST_MA.parse().unwrap();
        let mut ai = AuthorityInfo::new(TEST_AUTH_KEY.parse().unwrap());
        // We found out the peerId
        ai.peer_id = Some(TEST_PI.parse().unwrap());
        let _r = env.cache.store(&ai).await.unwrap();

        // Retrieve the Authority Info, with type inference
        let b = env.cache.retrieve(TEST_AUTH_KEY).await.unwrap().unwrap();
        assert_eq!(ai, b);
    }

    #[tokio::test]
    async fn can_enqueue_tasks_executions() {
        let env = CacheTestEnv::new().await;
        let a: Authority = TEST_AUTH_KEY.parse().unwrap();

        let t = TaskExecution {
            task: Task::Discover(a),
            expiry: Utc::now(),
            retries: 0,
        };

        // enque some tasks
        for _i in 1..=5 {
            env.cache.enqueue(&t).await.unwrap();
        }

        // check that the length is correct
        let l = env.cache.queue_length::<TaskExecution>().await.unwrap();
        assert_eq!(l, 5);

        // get them back from the queue
        for _i in 1..=5 {
            let t2 = env.cache.dequeue::<TaskExecution>().await.unwrap().unwrap();
            assert_eq!(t, t2);
        }

        // and now the queue should be empty
        let t3 = env.cache.dequeue::<TaskExecution>().await.unwrap();
        assert!(t3.is_none());

        // check that the length is correct
        let l = env.cache.queue_length::<TaskExecution>().await.unwrap();
        assert_eq!(l, 0);
    }
}
