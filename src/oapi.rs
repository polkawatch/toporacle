// Copyright 2024 Valletech AB authors & contributors
// SPDX-License-Identifier: Apache-2.0

use crate::cache::Cache;
use crate::cli::Args;
use crate::types::{Authority, AuthorityInfo, ChainInfo, ChainLocationStatus, KeyOwnerInfo};
use rocket::http::Status;
use rocket::{get, serde::json::Json, State};
use rocket_okapi::okapi::schemars;
use rocket_okapi::okapi::schemars::JsonSchema;
use rocket_okapi::rapidoc::{make_rapidoc, GeneralConfig, HideShowConfig, RapiDocConfig};
use rocket_okapi::settings::UrlObject;
use rocket_okapi::swagger_ui::{make_swagger_ui, SwaggerUIConfig};
use rocket_okapi::{openapi, openapi_get_routes, openapi_get_spec};
use serde::{Deserialize, Serialize};
use std::sync::Arc;

/// Our Application State
struct AppState {
    pub chain_info: Arc<ChainInfo>,
    pub cache: Cache,
}

impl AppState {
    fn new(chain_info: Arc<ChainInfo>, cache: Cache) -> Self {
        AppState { chain_info, cache }
    }
}

#[derive(Serialize, Deserialize, JsonSchema)]
#[serde(rename_all = "camelCase")]
struct Version {
    /// The version of TopOracle
    version: String,
}

/// # Version of toporacle
///
/// Returns the version of the toporacle rust crate.
#[openapi(tag = "Meta")]
#[get("/version")]
fn get_version() -> Json<Version> {
    Json(Version {
        version: env!("CARGO_PKG_VERSION").parse().unwrap(),
    })
}

/// # Chain Information
///
/// The chain information we are running with
#[openapi(tag = "Meta")]
#[get("/chain_info")]
fn get_chain_info(state: &State<AppState>) -> Json<&ChainInfo> {
    Json(&state.chain_info)
}

/// # Chain authority location status
///
/// The current status of the location activity
#[openapi(tag = "Status")]
#[get("/chain_stats")]
async fn get_chain_stats(state: &State<AppState>) -> Result<Json<ChainLocationStatus>, Status> {
    match state
        .cache
        .retrieve::<ChainLocationStatus>("location")
        .await
    {
        Ok(Some(cls)) => Ok(Json(cls)),
        Ok(None) => Err(Status::NotFound),
        // Server failuers
        Err(_) => Err(Status::InternalServerError),
    }
}

/// # Authority Discovery Information
///
/// Retrieves node addressing information by Authority Discovery Key encoded in hexadecimal string
/// prefixed by "0x".
#[openapi(tag = "Authority")]
#[get("/authority/<id>")]
async fn get_authority(state: &State<AppState>, id: &str) -> Result<Json<AuthorityInfo>, Status> {
    // try to parse the ID
    let a: Result<Authority, _> = id.parse();
    match a {
        Ok(_) => {
            match state.cache.retrieve::<AuthorityInfo>(id).await {
                // Content related reponses
                Ok(Some(ai)) => Ok(Json(ai)),
                Ok(None) => Err(Status::NotFound),
                // Server failuers
                Err(_) => Err(Status::InternalServerError),
            }
        }
        // We could not parse the ID, we assume is wrong.
        Err(_) => Err(Status::BadRequest),
    }
}

/// # Authority Information by Key Owner
///
/// Retrieves node addressing information by on-chain registered owner of the Authority Discovery Key.
/// This is typically the Account of commonly known as Validator, encoded in chain formatted
/// SS58 format.
///
#[openapi(tag = "Authority")]
#[get("/key_owner/<id>")]
async fn get_owner(state: &State<AppState>, id: &str) -> Result<Json<AuthorityInfo>, Status> {
    // Retrieve the KeyOwner
    match state.cache.retrieve::<KeyOwnerInfo>(id).await {
        Ok(Some(koi)) => {
            // we now search for the associated Authority Info
            match state
                .cache
                .retrieve::<AuthorityInfo>(koi.authority.to_hex().as_str())
                .await
            {
                // Content related reponses
                Ok(Some(ai)) => Ok(Json(ai)),
                Ok(None) => Err(Status::NotFound),
                // Server failures
                Err(_) => Err(Status::InternalServerError),
            }
        }
        Ok(None) => Err(Status::NotFound),
        Err(_) => Err(Status::InternalServerError),
    }
}

// Starts the OpenAPI server, will also generate the OpenAPI Spec,
// documentation site and swagger interface.
pub fn server(
    cfg: Arc<Args>,
    chain_info: Arc<ChainInfo>,
    cache: Cache,
) -> rocket::Rocket<rocket::Build> {
    rocket::build()
        .manage(AppState::new(chain_info, cache))
        .configure(rocket::Config {
            address: "0.0.0.0".parse().unwrap(),
            port: cfg.api_port,
            ..rocket::Config::default()
        })
        .mount(
            "/",
            openapi_get_routes![
                get_version,
                get_chain_info,
                get_chain_stats,
                get_authority,
                get_owner
            ],
        )
        .mount(
            "/swagger-ui/",
            make_swagger_ui(&SwaggerUIConfig {
                url: "../openapi.json".to_owned(),
                ..Default::default()
            }),
        )
        .mount(
            "/rapidoc/",
            make_rapidoc(&RapiDocConfig {
                general: GeneralConfig {
                    spec_urls: vec![UrlObject::new("General", "../openapi.json")],
                    ..Default::default()
                },
                hide_show: HideShowConfig {
                    allow_spec_url_load: false,
                    allow_spec_file_load: false,
                    ..Default::default()
                },
                ..Default::default()
            }),
        )
}

// Generates to openapi spec
pub fn spec() {
    let spec = openapi_get_spec![get_version, get_chain_info, get_authority, get_owner];
    let json = serde_json::to_string_pretty(&spec).unwrap();
    std::fs::write("openapi.json", json).expect("Could not write openapi.json");
}
