# Substrate Topology Oracle
This server will inspect the network topology of Substrate networks will make the information available via OpenAPI.

Toporacle Responsibilities:
 
- Connects to a Substrate Node
- Learns about key nodes of the p2p network
- Caches network topology information and renews it at reasonable intervals
- Can be queried by OpenAPI making the information available to analytic software 

TopOracle does not deliver the topology data to the blockchain yet. Ideally multiple independent TopOracle servers 
would deliver their metrics and the blockchain would register location metrics posted by the majority, or similar. 

# Software stack

Toporacle is build with the following main components:

- [Tokio](https://crates.io/crates/tokio): An event-driven, non-blocking I/O platform for writing asynchronous I/O backed applications. 
- [libp2p](https://crates.io/crates/libp2p): Peer-to-peer networking library; which is also used to implement substrate p2p network. 
- [subxt](https://crates.io/crates/subxt):  Submit extrinsics (transactions) to a substrate node via RPC; which also allows to query state and subscribe to events.
- [rocket](https://crates.io/crates/rocket) and [rocket_okapi](https://crates.io/crates/rocket_okapi):  Web framework with a focus on usability, security, extensibility, and speed;  with OpenAPI (AKA Swagger) document generation for Rocket applications.
- [clap](https://crates.io/crates/clap):  A simple to use, efficient, and full-featured Command Line Argument Parser. 
- [tracing](https://crates.io/crates/tracing):  Application-level tracing for Rust. 
- [redis](https://crates.io/crates/redis):  Redis driver for Rust; which caches network topology information.
- [subp2p-explorer](https://github.com/lexnv/subp2p-explorer):  Substrate based chains p2p network explorer.

Toporacle concurrency is developed using Asynchronous programming.  

# License and Copyright

Toporacle is part of the [Polkawatch](https://polkawatch.app/) project, funded the Polkadot community.

Toporacle is Open Source, Apache License Version 2.0.

©2024 [Valletech AB](https://valletech.eu), Authors and Contributors.