stages:
  - build
  - release
  - deploy
  - client_build
  - client_deploy

toporacle build:
  stage: build
  image: rust
  services:
    - name: redis:7
      alias: redis
  variables:
    RUST_BACKTRACE: 1
    CARGO_INCREMENTAL: 0
    CARGO_NET_RETRY: 10
    RUSTFLAGS: -D warnings
    RUSTUP_MAX_RETRIES: 10
    CARGO_TERM_COLOR: always
    TOPO_REDIS: "redis://redis/"
  before_script:
    - apt update
    - apt install -y protobuf-compiler
    - cargo install cargo-make
  script:
    - cargo make all

toporacle release:
  stage: release
  image: rust
  before_script:
    # Setup Git for Push
    - apt update
    - apt install -y openssh-client git protobuf-compiler jq
    - git config --global user.email ${CI_ROBOT_EMAIL}
    - git config --global user.name "CI Robot"
    - git remote remove porigin || echo Ignored.
    - git remote add porigin https://${CI_ROBOT_USERNAME}:${CI_ROBOT_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git
    - cargo install cargo-edit cargo-make
  script:
    # Increase the release number
    - cargo set-version --bump patch
    # Commit and Tag the repository
    - APP_VERSION=$(cargo metadata --no-deps --format-version 1 | jq -r '.packages[0].version')
    - git commit . -m "Released version ${APP_VERSION}"
    - git tag v${APP_VERSION}
    # Generate the openapi specification
    - cargo make build_spec
    # Push to the branch the triggered
    - git push porigin HEAD:${CI_COMMIT_REF_NAME} --tags -o ci.skip
    # Pass the version to the next stage
    - echo ${APP_VERSION} > .APP_VERSION
  when: manual
  allow_failure: false
  only:
    - main
    - /^stable.*$/
  artifacts:
    paths:
      - .APP_VERSION
      - Cargo.toml
      - openapi.json

toporacle docker image deployment:
  stage: deploy
  dependencies:
    - toporacle release
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  before_script:
    # Create the docker registry credentials
    - |
      cat > /kaniko/.docker/config.json <<EOF
      {
        "auths": {
          "https://index.docker.io/v1/": {
            "username": "${DOCKER_REGISTRY_USER}",
            "password": "${DOCKER_REGISTRY_PASSWORD}",
            "auth": "$(echo -n "${DOCKER_REGISTRY_USER}:${DOCKER_REGISTRY_PASSWORD}" | base64)"
          }
        }
      }
      EOF
  script:
    # Get the version from the artifacts
    - APP_VERSION=$(cat .APP_VERSION)
    # Build and push the docker container
    - > 
      /kaniko/executor 
      --context "${CI_PROJECT_DIR}" 
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile" 
      --destination "valletech/toporacle:${APP_VERSION}"
      --destination "valletech/toporacle:latest"
  only:
    - main
    - /^stable.*$/

toporacle client generation:
  stage: client_build
  dependencies:
    - toporacle release
  image: openapitools/openapi-generator-cli
  variables:
    OPENAPI_GENERATOR_CLI: java -jar /opt/openapi-generator/modules/openapi-generator-cli/target/openapi-generator-cli.jar
  script:
    - mkdir -p client
    - APP_VERSION=$(cat .APP_VERSION)
    - >
      ${OPENAPI_GENERATOR_CLI} generate 
      -i ./openapi.json 
      --generator-name typescript-axios 
      -o ./client 
      --additional-properties=useSingleRequestParameter=true,npmName=@polkawatch/toporacle-client
  artifacts:
    paths:
      - client
  only:
    - main
    - /^stable.*$/

toporacle client deployment:
  stage: client_deploy
  dependencies:
    - toporacle client generation
  image: node:18-alpine
  script:
    - cd client
    # Setup npm publishing
    - echo //registry.npmjs.org/:_authToken=\${NPM_TOKEN} > .npmrc
    # Build and publish the client
    - yarn install
    - yarn publish --access public
  only:
    - main
    - /^stable.*$/
